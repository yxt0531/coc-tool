extends ItemList

var ids = []

func _ready():
	for character_id in CharacterDB.characters.keys():
		ids.append(character_id)
		add_item(CharacterDB.characters[character_id].name)

func _on_CharacterList_item_selected(index):
	get_parent().select_character(ids[index])

func _on_CharacterList_nothing_selected():
	get_parent().deselect_character()

func refresh():
	ids.clear()
	clear()
	for character_id in CharacterDB.characters.keys():
		ids.append(character_id)
		add_item(CharacterDB.characters[character_id].name)
