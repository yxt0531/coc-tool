extends Control

var y_position

func _ready():
	y_position = rect_position.y

func _process(delta):
	if Input.is_action_just_pressed("ui_scroll_up"):
		get_parent().get_node("DetailsScrollBar").value -= 10
	elif Input.is_action_just_pressed("ui_scroll_down"):
		get_parent().get_node("DetailsScrollBar").value += 10
		
func _on_DetailsScrollBar_value_changed(value):
	rect_position.y = y_position - value * 10

func populate_details(character):
	$InputName.text = str(character.name)
	$InputAge.text = str(character.age)
	$InputSex.text = str(character.sex)
	$InputPlayer.text = str(character.player)
	$InputProfession.text = str(character.profession)
	$InputAddress.text = str(character.address)
	$InputOrigin.text = str(character.origin)
	
	$InputStr.text = str(character.str)
	$InputDex.text = str(character.dex)
	$InputInt.text = str(character.int)
	$InputCon.text = str(character.con)
	$InputApp.text = str(character.app)
	$InputPow.text = str(character.pow)
	$InputSiz.text = str(character.siz)
	$InputEdu.text = str(character.edu)
	$InputLuck.text = str(character.luck)
	
	calculate_sum()
	
	$Appearance/TextEdit.text = str(character.bg_appearance)
	$Belief/TextEdit.text = str(character.bg_belief)
	$Person/TextEdit.text = str(character.bg_person)
	$Place/TextEdit.text = str(character.bg_place)
	$Treasure/TextEdit.text = str(character.bg_treasure)
	$Personality/TextEdit.text = str(character.bg_personality)
	$Scar/TextEdit.text = str(character.bg_scar)
	$Illness/TextEdit.text = str(character.bg_illness)
	$Extra/TextEdit.text = str(character.bg_extra)
	$Assets/TextEdit.text = str(character.assets)

func calculate_sum():
	$InputSum.text = str(int($InputStr.text) + int($InputDex.text) + int($InputInt.text) + int($InputCon.text) + int($InputApp.text) + int($InputPow.text) + int($InputSiz.text) + int($InputEdu.text))
	
