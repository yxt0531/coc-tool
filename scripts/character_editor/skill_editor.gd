extends ColorRect

var skill_indices = []

func populate_skill_list(skills):
	skill_indices.clear()
	$SkillList.clear()
	for skill in skills.keys():
		skill_indices.append(skill)
		$SkillList.add_item(skill + ": " + str(skills[skill].base) + ", " + str(skills[skill].pro) + ", " + str(skills[skill].interest))
	$InputSkillBase.text = ""
	$InputSkillPro.text = ""
	$InputSkillInterest.text = ""
	$InputSkillName.text = ""
	$ButtonAdd.disabled = true
	$ButtonRemove.disabled = true
	$ButtonSave.disabled = true

func _is_skill_exist(skill):
	return get_parent().selected_character.skills.has(skill)

func _on_SkillList_item_selected(index):
	$InputSkillName.text = skill_indices[index]
	$InputSkillBase.text = str(get_parent().selected_character.skills[skill_indices[index]].base)
	$InputSkillPro.text = str(get_parent().selected_character.skills[skill_indices[index]].pro)
	$InputSkillInterest.text = str(get_parent().selected_character.skills[skill_indices[index]].interest)
	$ButtonAdd.disabled = true
	$ButtonSave.disabled = true
	$ButtonRemove.disabled = false

func _on_InputSkillBase_text_changed(new_text):
	if not Validator.is_number(new_text):
		$InputSkillBase.text = ""
	else:
		_change_listener()

func _on_InputSkillPro_text_changed(new_text):
	if not Validator.is_number(new_text):
		$InputSkillPro.text = ""
	else:
		_change_listener()

func _on_InputSkillInterest_text_changed(new_text):
	if not Validator.is_number(new_text):
		$InputSkillInterest.text = ""
	else:
		_change_listener()

func _on_InputSkillName_text_changed(new_text):
	_change_listener()
	if new_text == "":
		$ButtonAdd.disabled = true
		$ButtonRemove.disabled = true
		$ButtonSave.disabled = true
		$InputSkillBase.text = ""
		$InputSkillPro.text = ""
		$InputSkillInterest.text = ""
	else:
		if _is_skill_exist(new_text):
			$ButtonAdd.disabled = true
			$ButtonRemove.disabled = false
		else:
			$ButtonAdd.disabled = false
			$ButtonRemove.disabled = true
		
func _change_listener():
	if _is_skill_exist($InputSkillName.text):
		if get_parent().selected_character.skills[$InputSkillName.text].base == int($InputSkillBase.text) and get_parent().selected_character.skills[$InputSkillName.text].pro == int($InputSkillPro.text) and get_parent().selected_character.skills[$InputSkillName.text].interest == int($InputSkillInterest.text):
			$ButtonSave.disabled = true
		else:
			$ButtonSave.disabled = false
	else:
		$ButtonSave.disabled = true

func _on_ButtonAdd_button_down():
	get_parent().selected_character.skills[$InputSkillName.text] = {}
	get_parent().selected_character.skills[$InputSkillName.text].base = int($InputSkillBase.text)
	get_parent().selected_character.skills[$InputSkillName.text].pro = int($InputSkillPro.text)
	get_parent().selected_character.skills[$InputSkillName.text].interest = int($InputSkillInterest.text)
	get_parent()._on_ButtonEditSkill_button_down()

func _on_ButtonSave_button_down():
	get_parent().selected_character.skills[$InputSkillName.text].base = int($InputSkillBase.text)
	get_parent().selected_character.skills[$InputSkillName.text].pro = int($InputSkillPro.text)
	get_parent().selected_character.skills[$InputSkillName.text].interest = int($InputSkillInterest.text)
	get_parent()._on_ButtonEditSkill_button_down()

func _on_ButtonRemove_button_down():
	get_parent().selected_character.skills.erase($InputSkillName.text)
	get_parent()._on_ButtonEditSkill_button_down()

func _on_ButtonReturn_button_down():
	hide()
