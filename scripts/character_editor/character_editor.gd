extends Control

var selected_character_id
var selected_character
var selected_character_skills

func _ready():
	pass

func select_character(index):
	selected_character_id = index
	selected_character = CharacterDB.characters[selected_character_id].duplicate(true)
	$DetailsView/Details.populate_details(selected_character)
	$DetailsView.show()
	
	$ButtonEditSkill.disabled = false
	$ButtonDelete.disabled = false
	$ButtonSave.disabled = false
	
func deselect_character():
	$DetailsView.hide()
	selected_character_id = null
	if selected_character != null:
		selected_character.clear()

	$ButtonEditSkill.disabled = true
	$ButtonDelete.disabled = true
	$ButtonSave.disabled = true

func _on_ButtonReturn_button_down():
	get_tree().change_scene("res://scenes/main_menu.tscn")

func _on_ButtonDelete_button_down():
	# delete character
	CharacterDB.delete_character(selected_character_id)
	deselect_character()
	$CharacterList.refresh()
	$LabelStatus.change_text("角色已删除")

func _on_ButtonSave_button_down():
	selected_character.name = $DetailsView/Details/InputName.text
	selected_character.age = int($DetailsView/Details/InputAge.text)
	selected_character.sex = $DetailsView/Details/InputSex.text
	selected_character.player = $DetailsView/Details/InputPlayer.text
	selected_character.profession = $DetailsView/Details/InputProfession.text
	selected_character.address = $DetailsView/Details/InputAddress.text
	selected_character.origin = $DetailsView/Details/InputOrigin.text
	
	selected_character.str = int($DetailsView/Details/InputStr.text)
	selected_character.dex = int($DetailsView/Details/InputDex.text)
	selected_character.int = int($DetailsView/Details/InputInt.text)
	selected_character.con = int($DetailsView/Details/InputCon.text)
	selected_character.app = int($DetailsView/Details/InputApp.text)
	selected_character.pow = int($DetailsView/Details/InputPow.text)
	selected_character.siz = int($DetailsView/Details/InputSiz.text)
	selected_character.edu = int($DetailsView/Details/InputEdu.text)
	selected_character.luck = int($DetailsView/Details/InputLuck.text)
	
	selected_character.bg_appearance = $DetailsView/Details/Appearance/TextEdit.text
	selected_character.bg_belief = $DetailsView/Details/Belief/TextEdit.text
	selected_character.bg_person = $DetailsView/Details/Person/TextEdit.text
	selected_character.bg_place = $DetailsView/Details/Place/TextEdit.text
	selected_character.bg_treasure = $DetailsView/Details/Treasure/TextEdit.text
	selected_character.bg_personality = $DetailsView/Details/Personality/TextEdit.text
	selected_character.bg_scar = $DetailsView/Details/Scar/TextEdit.text
	selected_character.bg_illness = $DetailsView/Details/Illness/TextEdit.text
	selected_character.bg_extra = $DetailsView/Details/Extra/TextEdit.text
	selected_character.assets = $DetailsView/Details/Assets/TextEdit.text
	
	CharacterDB.replace_character(selected_character_id, selected_character)
	deselect_character()
	$CharacterList.refresh()
	$LabelStatus.change_text("角色已保存")

func _on_ButtonEditSkill_button_down():
	$SkillEditor.populate_skill_list(selected_character.skills)
	$SkillEditor.show()
