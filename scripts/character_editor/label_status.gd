extends Label

var cd_timer = 0.0

func _process(delta):
	if cd_timer > 0:
		cd_timer -= delta
	else:
		text = ""

func change_text(text):
	cd_timer = 5
	self.text = str(text)
