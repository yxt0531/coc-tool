extends LineEdit

func _on_Input_text_changed(new_text):
	if not Validator.is_number(new_text):
		text = ""
	else:
		get_parent().calculate_sum()

