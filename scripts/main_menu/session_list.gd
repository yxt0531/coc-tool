extends ItemList

var ids = []
var session_id

func refresh():
	ids.clear()
	clear()
	for session_id in SessionDB.sessions.keys():
		ids.append(session_id)
		add_item(SessionDB.sessions[session_id].name)
	_on_SessionList_nothing_selected()

func _on_SessionList_item_selected(index):
	get_parent().get_node("ButtonStart").text = "载入游戏"
	Current.session_id = ids[index]
	get_parent().get_node("ButtonDelete").disabled = false

func _on_SessionList_nothing_selected():
	Current.session_id = null
	get_parent().get_node("ButtonStart").text = "开始游戏"
	get_parent().get_node("ButtonDelete").disabled = true
