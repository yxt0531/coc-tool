extends ItemList
# character list on main menu
var ids = []

func refresh():
	ids.clear()
	clear()
	for character_id in CharacterDB.characters.keys():
		ids.append(character_id)
		add_item(CharacterDB.characters[character_id].name)

func _on_CharacterList_item_selected(index):
	Current.character_id = ids[index]
	get_parent().get_node("ButtonJoin").disabled = false

func _on_CharacterList_nothing_selected():
	get_parent().get_node("ButtonJoin").disabled = true
