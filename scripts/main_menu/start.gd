extends ColorRect

func refresh():
	match Current.type:
		Current.Type.CLIENT:
			$InputIP.editable = true
			$ButtonPlayAs.text = "作为调查员: " + CharacterDB.characters[Current.character_id].name + " 加入游戏"
			
			if Validator.is_ip_address($InputIP.text) and Validator.is_number($InputPort.text) and $InputPort.text != "":
				$ButtonPlayAs.disabled = false
			else:
				$ButtonPlayAs.disabled = true
		Current.Type.HOST:
			$InputIP.editable = false
			$ButtonPlayAs.text = "作为守秘人开始游戏"
			
			if Validator.is_number($InputPort.text) and $InputPort.text != "":
				$ButtonPlayAs.disabled = false
			else:
				$ButtonPlayAs.disabled = true
	
func _on_ButtonPrev_button_down():
	hide()

func _on_ButtonPlayAs_button_down():
	Current.port = $InputPort.text
	match Current.type:
		Current.Type.CLIENT:
			Current.ip = $InputIP.text
			Current.character = CharacterDB.characters[Current.character_id].duplicate(true)
			print("joining game as client: " + Current.ip + ":" + Current.port + " with character: " + Current.character.name)
			get_tree().change_scene("res://scenes/lobby.tscn")
		Current.Type.HOST:
			print("starting game as host, port: " + Current.port)
			get_tree().change_scene("res://scenes/lobby.tscn")

func _on_InputIP_text_changed(new_text):
	refresh()

func _on_InputPort_text_changed(new_text):
	if not Validator.is_number($InputPort.text):
		$InputPort.text = ""
	refresh()
