extends ColorRect

func _ready():
	# load characters
	if CharacterDB.characters.size() <= 0:
		$LabelHelpText.text = "你目前没有角色，请创建角色后开始游戏。"
	else:
		$LabelHelpText.text = "作为调查员加入游戏前请在左侧选择你的角色"
		$ButtonEditCharacter.disabled = false
		$CharacterList.refresh()
		
	# load sessions
	if not SessionDB.sessions.size() <= 0:
		$SessionList.refresh()
	
	if Current.ip != null:
		$Start/InputIP.text = Current.ip
		
	if Current.port != null:
		$Start/InputPort.text = Current.port

func _on_ButtonStart_button_down():
	Current.type = Current.Type.HOST
	$Start.show()
	$Start.refresh()

func _on_ButtonJoin_button_down():
	Current.type = Current.Type.CLIENT
	$Start.show()
	$Start.refresh()

func _on_ButtonCreateCharacter_button_down():
	get_tree().change_scene("res://scenes/character_creation.tscn")

func _on_ButtonEditCharacter_button_down():
	get_tree().change_scene("res://scenes/character_editor.tscn")

func _on_ButtonExit_button_down():
	get_tree().quit()

func _on_ButtonDelete_button_down():
	SessionDB.delete_session(Current.session_id)
	Current.session_id = null
	$SessionList.refresh()
	
func _on_ButtonHelp_button_down():
	get_tree().change_scene("res://scenes/help.tscn")
