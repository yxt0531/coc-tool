extends Control

func _on_CharacterCreation_button_down():
	get_tree().change_scene("res://scenes/character_creation.tscn")

func _on_ListAllCharacterAsJSON_button_down():
	var json_string = JSON.print(CharacterDB.characters, "    ")
	_append_to_console(json_string)

func _on_ClearConsole_button_down():
	_clear_console()
	
func _append_to_console(text):
	$Console.text = $Console.text + text
	
func _clear_console():
	$Console.text = ""

func _on_DeleteAllCharacters_button_down():
	CharacterDB.clear()
