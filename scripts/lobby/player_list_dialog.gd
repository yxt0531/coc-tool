extends WindowDialog

var remote_character
var currect_selected_character_index

func _ready():
	$DetailList.add_item("形象描述")
	$DetailList.add_item("思想与信念")
	$DetailList.add_item("重要之人")
	$DetailList.add_item("意义非凡之地")
	$DetailList.add_item("宝贵之物")
	$DetailList.add_item("特质")
	$DetailList.add_item("伤口和伤疤")
	$DetailList.add_item("恐惧症和狂躁症")
	$DetailList.add_item("额外背景")
	$DetailList.add_item("资产，装备和道具")

func _on_ListPlayer_button_down():
	match Current.type:
		Current.Type.CLIENT:
			$ButtonSkillAdd.disabled = true
			$ButtonSkillRemove.disabled = true
			
			$STR.editable = false
			$DEX.editable = false
			$INT.editable = false
			$CON.editable = false
			$APP.editable = false
			$POW.editable = false
			$SIZ.editable = false
			$EDU.editable = false
			$Luck.editable = false
			$SkillBase.editable = false
			$SkillPro.editable = false
			$SkillInterest.editable = false
			$SkillName.editable = false
			
			$HP/Current.editable = false
			$HP/MAX.editable = false
			$MP/Current.editable = false
			$MP/MAX.editable = false
			$SAN/Current.editable = false
			$SAN/MAX.editable = false
			$Damage.editable = false
			$Body.editable = false
			$MOV.editable = false
			
			$Name.editable = false
			$Player.editable = false
			$Job.editable = false
			$Address.editable = false
			$Origin.editable = false
			$Age.editable = false
			$Sex.editable = false
			
			$BGAppearance/Content.readonly = true
			$BGBelief/Content.readonly = true
			$BGPerson/Content.readonly = true
			$BGPlace/Content.readonly = true
			$BGTreasure/Content.readonly = true
			$BGPersonality/Content.readonly = true
			$BGScar/Content.readonly = true
			$BGIllness/Content.readonly = true
			$BGExtra/Content.readonly = true
			$Assets/Content.readonly = true
			_request_remote_character()
			
		Current.Type.HOST:
			_populate_connected_player_list()
			_refresh_labels()
			popup_centered()

func _populate_connected_player_list():
	$PlayerList.clear()
	for id in Current.connected_players:
		$PlayerList.add_item(Current.connected_characters[id].name)

func _request_remote_character():
	get_parent().rpc_id(1, "_character_request_received", get_parent().peer.get_unique_id())

func display_remote_character(character):
	print("remote character detail received")
	popup_centered()
	# display character info here
	$STR.text = str(character.str)
	$DEX.text = str(character.dex)
	$INT.text = str(character.int)
	$CON.text = str(character.con)
	$APP.text = str(character.app)
	$POW.text = str(character.pow)
	$SIZ.text = str(character.siz)
	$EDU.text = str(character.edu)
	$Luck.text = str(character.luck)
	
	$HP/Current.text = str(character.hp)
	$HP/MAX.text = str(character.hp_max)
	
	$MP/Current.text = str(character.mp)
	$MP/MAX.text = str(character.mp_max)
	
	$SAN/Current.text = str(character.san)
	$SAN/MAX.text = str(character.san_max)
	
	$Damage.text = character.dmg
	$Body.text = str(character.body)
	$MOV.text = str(character.mov)
	
	$Name.text = character.name
	$Player.text = character.player
	$Job.text = character.job
	$Address.text = character.address
	$Origin.text = character.origin
	$Age.text = str(character.age)
	$Sex.text = character.sex
	
	$BGAppearance/Content.text = character.bg_appearance
	$BGBelief/Content.text = character.bg_belief
	$BGPerson/Content.text = character.bg_person
	$BGPlace/Content.text = character.bg_place
	$BGTreasure/Content.text = character.bg_treasure
	$BGPersonality/Content.text = character.bg_personality
	$BGScar/Content.text = character.bg_scar
	$BGIllness/Content.text = character.bg_illness
	$BGExtra/Content.text = character.bg_extra
	$Assets/Content.text = character.assets
	
	$SkillList.clear()
	for skill in character.skills.keys():
		$SkillList.add_item(skill)
	
	remote_character = character
	_refresh_labels()

func _refresh_labels():
	$STR/Label.text = "力量: " + str(int($STR.text)) + ", " + str(int($STR.text) / 2) + ", " + str(int($STR.text) / 5)
	$DEX/Label.text = "敏捷: " + str(int($DEX.text)) + ", " + str(int($DEX.text) / 2) + ", " + str(int($DEX.text) / 5)
	$INT/Label.text = "智力: " + str(int($INT.text)) + ", " + str(int($INT.text) / 2) + ", " + str(int($INT.text) / 5)
	$CON/Label.text = "体质: " + str(int($CON.text)) + ", " + str(int($CON.text) / 2) + ", " + str(int($CON.text) / 5)
	$APP/Label.text = "外貌: " + str(int($APP.text)) + ", " + str(int($APP.text) / 2) + ", " + str(int($APP.text) / 5)
	$POW/Label.text = "意志: " + str(int($POW.text)) + ", " + str(int($POW.text) / 2) + ", " + str(int($POW.text) / 5)
	$SIZ/Label.text = "体型: " + str(int($SIZ.text)) + ", " + str(int($SIZ.text) / 2) + ", " + str(int($SIZ.text) / 5)
	$EDU/Label.text = "教育: " + str(int($EDU.text)) + ", " + str(int($EDU.text) / 2) + ", " + str(int($EDU.text) / 5)
	$Luck/Label.text = "幸运: " + str(int($Luck.text)) + ", " + str(int($Luck.text) / 2) + ", " + str(int($Luck.text) / 5)

	if $SkillBase.text == "" or $SkillPro.text == "" or $SkillInterest.text == "":
		$SkillBase/Label.text = "初始: 0, 0, 0"
		$SkillPro/Label.text = "职业: 0, 0, 0"
		$SkillInterest/Label.text = "兴趣: 0, 0, 0"
	else:
		$SkillBase/Label.text = "初始: " + $SkillBase.text + ", " + str(int($SkillBase.text) / 2) + ", " + str(int($SkillBase.text) / 5)
		$SkillPro/Label.text = "职业: " + $SkillPro.text + ", " + str(int($SkillPro.text) / 2) + ", " + str(int($SkillPro.text) / 5)
		$SkillInterest/Label.text = "兴趣: " + $SkillInterest.text + ", " + str(int($SkillInterest.text) / 2) + ", " + str(int($SkillInterest.text) / 5)

func _populate_selected_player_skill_list(index):
	$SkillList.clear()
	for skill in Current.connected_characters[Current.connected_players[index]].skills.keys():
		$SkillList.add_item(skill)
		
func _on_PlayerList_item_selected(index):
	match Current.type:
		Current.Type.HOST:
			currect_selected_character_index = index
			_populate_selected_player_skill_list(currect_selected_character_index)
			
			# display character info here
			$STR.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].str)
			$DEX.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].dex)
			$INT.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].int)
			$CON.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].con)
			$APP.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].app)
			$POW.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].pow)
			$SIZ.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].siz)
			$EDU.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].edu)
			$Luck.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].luck)
			
			$HP/Current.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].hp)
			$HP/MAX.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].hp_max)
			
			$MP/Current.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].mp)
			$MP/MAX.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].mp_max)
			
			$SAN/Current.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].san)
			$SAN/MAX.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].san_max)
			
			$Damage.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].dmg
			$Body.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].body)
			$MOV.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].mov)
			
			$Name.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].name
			$Player.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].player
			$Job.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].job
			$Address.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].address
			$Origin.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].origin
			$Age.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].age)
			$Sex.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].sex
			
			$BGAppearance/Content.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_appearance
			$BGBelief/Content.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_belief
			$BGPerson/Content.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_person
			$BGPlace/Content.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_place
			$BGTreasure/Content.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_treasure
			$BGPersonality/Content.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_personality
			$BGScar/Content.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_scar
			$BGIllness/Content.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_illness
			$BGExtra/Content.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_extra
			$Assets/Content.text = Current.connected_characters[Current.connected_players[currect_selected_character_index]].assets
			
			_refresh_labels()
			$SkillName.editable = true
			$SkillName.text = ""
			_on_SkillName_text_changed("")
			$ButtonSkillKick.disabled = false

func _on_SkillList_item_selected(index):
	$SkillName.text = $SkillList.get_item_text(index)
	match Current.type:
		Current.Type.CLIENT:
			$SkillBase.text = str(remote_character.skills[$SkillName.text].base)
			$SkillPro.text = str(remote_character.skills[$SkillName.text].pro)
			$SkillInterest.text = str(remote_character.skills[$SkillName.text].interest)
			_refresh_labels()
		Current.Type.HOST:
			$SkillBase.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills[$SkillList.get_item_text(index)].base)
			$SkillPro.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills[$SkillList.get_item_text(index)].pro)
			$SkillInterest.text = str(Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills[$SkillList.get_item_text(index)].interest)
			_refresh_labels()
			_on_SkillName_text_changed("")

func _on_DetailList_item_selected(index):
	$BGAppearance.hide()
	$BGBelief.hide()
	$BGPerson.hide()
	$BGPlace.hide()
	$BGTreasure.hide()
	$BGPersonality.hide()
	$BGScar.hide()
	$BGIllness.hide()
	$BGExtra.hide()
	$Assets.hide()
	
	match index:
		0:
			$BGAppearance.show()
		1:
			$BGBelief.show()
		2:
			$BGPerson.show()
		3:
			$BGPlace.show()
		4:
			$BGTreasure.show()
		5:
			$BGPersonality.show()
		6:
			$BGScar.show()
		7:
			$BGIllness.show()
		8:
			$BGExtra.show()
		9:
			$Assets.show()

func _on_DetailList_nothing_selected():
	$BGAppearance.hide()
	$BGBelief.hide()
	$BGPerson.hide()
	$BGPlace.hide()
	$BGTreasure.hide()
	$BGPersonality.hide()
	$BGScar.hide()
	$BGIllness.hide()
	$BGExtra.hide()
	$Assets.hide()

func _on_PlayerListDialog_popup_hide():
	$ButtonSkillAdd.disabled = true
	$ButtonSkillRemove.disabled = true
	$ButtonSkillKick.disabled = true
	
	$STR.text = ""
	$DEX.text = ""
	$INT.text = ""
	$CON.text = ""
	$APP.text = ""
	$POW.text = ""
	$SIZ.text = ""
	$EDU.text = ""
	$Luck.text = ""
	
	$HP/Current.text = ""
	$HP/MAX.text = ""
	
	$MP/Current.text = ""
	$MP/MAX.text = ""
	
	$SAN/Current.text = ""
	$SAN/MAX.text = ""
	
	$Damage.text = ""
	$Body.text = ""
	$MOV.text = ""
	
	$Name.text = ""
	$Player.text = ""
	$Job.text = ""
	$Address.text = ""
	$Origin.text = ""
	$Age.text = ""
	$Sex.text = ""
	
	$BGAppearance/Content.text = ""
	$BGBelief/Content.text = ""
	$BGPerson/Content.text = ""
	$BGPlace/Content.text = ""
	$BGTreasure/Content.text = ""
	$BGPersonality/Content.text = ""
	$BGScar/Content.text = ""
	$BGIllness/Content.text = ""
	$BGExtra/Content.text = ""
	$Assets/Content.text = ""

	$SkillName.editable = false
	$SkillName.text = ""
	$SkillBase.text = ""
	$SkillPro.text = ""
	$SkillInterest.text = ""
	
	$SkillList.clear()
	$PlayerList.clear()
	currect_selected_character_index = null

func _is_skill_exist(skill):
	if currect_selected_character_index == null:
		return false
	else:
		return Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills.has(skill)

func _on_SkillName_text_changed(new_text):
	if $SkillName.text == "":
		$ButtonSkillAdd.disabled = true
		$ButtonSkillRemove.disabled = true
	elif _is_skill_exist($SkillName.text):
		$ButtonSkillAdd.disabled = true
		$ButtonSkillRemove.disabled = false
	else:
		$ButtonSkillAdd.disabled = false
		$ButtonSkillRemove.disabled = true

func _on_ButtonReturn_button_down():
	hide()

func _on_stats_text_changed(new_text):
	# numbered stat changed
	if currect_selected_character_index != null and Current.type == Current.Type.HOST:
		if Validator.is_number(new_text):
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].str = int($STR.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].dex = int($DEX.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].int = int($INT.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].con = int($CON.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].app = int($APP.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].pow = int($POW.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].siz = int($SIZ.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].edu = int($EDU.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].luck = int($Luck.text)
	
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].hp = int($HP/Current.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].hp_max = int($HP/MAX.text)
	
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].mp = int($MP/Current.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].mp_max = int($MP/MAX.text)
	
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].san = int($SAN/Current.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].san_max = int($SAN/MAX.text)
	
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].body = int($Body.text)
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].mov = int($MOV.text)
	
			Current.connected_characters[Current.connected_players[currect_selected_character_index]].age = int($Age.text)
			_refresh_labels()
		else:
			_on_PlayerList_item_selected(currect_selected_character_index)

func _on_text_stats_text_changed(new_text):
	# string based stat changed
	if currect_selected_character_index != null and Current.type == Current.Type.HOST:
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].dmg = $Damage.text

		Current.connected_characters[Current.connected_players[currect_selected_character_index]].name = $Name.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].player = $Player.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].job = $Job.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].address = $Address.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].origin = $Origin.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].sex = $Sex.text

func _on_bg_info_text_changed():
	# background info and asset changed
	if currect_selected_character_index != null and Current.type == Current.Type.HOST:
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_appearance = $BGAppearance/Content.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_belief = $BGBelief/Content.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_person = $BGPerson/Content.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_place = $BGPlace/Content.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_treasure = $BGTreasure/Content.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_personality = $BGPersonality/Content.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_scar = $BGScar/Content.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_illness = $BGIllness/Content.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].bg_extra = $BGExtra/Content.text
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].assets = $Assets/Content.text

func _on_ButtonSkillKick_button_down():
	if currect_selected_character_index != null and Current.type == Current.Type.HOST:
		get_parent()._append_player_log("[b]守秘人[/b]", Current.connected_characters[Current.connected_players[currect_selected_character_index]].name + "被移除了游戏。")
		get_parent().rpc("_append_player_log", "[b]守秘人[/b]", Current.connected_characters[Current.connected_players[currect_selected_character_index]].name + "被移除了游戏。")
		get_parent().rpc_id(Current.connected_players[currect_selected_character_index], "_kicked")
		get_parent().rpc("_connected_players_changed")
		if Current.connected_characters.erase(Current.connected_players[currect_selected_character_index]):
			print("player removed from game")

	hide()

func _on_skill_stat_text_changed(new_text):
	if currect_selected_character_index != null and Current.type == Current.Type.HOST:
		if _is_skill_exist($SkillName.text):
			if Validator.is_number(new_text):
				Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills[$SkillName.text].base = int($SkillBase.text)
				Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills[$SkillName.text].pro = int($SkillPro.text)
				Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills[$SkillName.text].interest = int($SkillInterest.text)
				_refresh_labels()
			else:
				_on_PlayerList_item_selected(currect_selected_character_index)

func _on_ButtonSkillAdd_button_down():
	if currect_selected_character_index != null and Current.type == Current.Type.HOST:
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills[$SkillName.text] = {}
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills[$SkillName.text].base = int($SkillBase.text)
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills[$SkillName.text].pro = int($SkillPro.text)
		Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills[$SkillName.text].interest = int($SkillInterest.text)
		_on_PlayerList_item_selected(currect_selected_character_index)

func _on_ButtonSkillRemove_button_down():
	if currect_selected_character_index != null and Current.type == Current.Type.HOST:
		if Current.connected_characters[Current.connected_players[currect_selected_character_index]].skills.erase($SkillName.text):
			print("skill erased by host")
			_on_PlayerList_item_selected(currect_selected_character_index)
