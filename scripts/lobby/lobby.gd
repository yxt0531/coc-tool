extends Control

const MAX_PLAYERS: int = 32

var peer # enet instance

func _process(delta):
	if Input.is_action_just_pressed("debug"):
		print(Current.connected_players)
	if Input.is_action_just_pressed("player_send"):
		_on_SendAsPlayer_button_down()
	elif Input.is_action_just_pressed("character_send"):
		_on_SendAsCharacter_button_down()
	elif Input.is_action_just_pressed("d4"):
		_on_D4_button_down()
	elif Input.is_action_just_pressed("d6"):
		_on_D6_button_down()
	elif Input.is_action_just_pressed("d8"):
		_on_D8_button_down()
	elif Input.is_action_just_pressed("d20"):
		_on_D20_button_down()
	elif Input.is_action_just_pressed("d100"):
		_on_D100_button_down()

func _load():
	if Current.session_id != null:
		$CharacterLog/Content.bbcode_text = SessionDB.sessions[Current.session_id].character_log
		$PlayerLog/Content.bbcode_text = SessionDB.sessions[Current.session_id].player_log
		$NotepadDialog/Notes.text = SessionDB.sessions[Current.session_id].notes
		Current.connected_characters = SessionDB.sessions[Current.session_id].characters.duplicate(true)
		
		Current.session_id = null
		print("session loaded")

func _exit_tree():
	# Current.save_to_session($CharacterLog/Content.bbcode_text, $PlayerLog/Content.bbcode_text, $NotepadDialog/Notes.text, "自动保存 " + Current.get_time_stamp())
	if Current.type == Current.Type.HOST:
		UPNPRequester.remove_port(int(Current.port))
	Current.clear()

func _ready():
	peer = NetworkedMultiplayerENet.new()
	
	if Current.type == Current.Type.HOST:
		peer.create_server(int(Current.port), MAX_PLAYERS)
		get_tree().set_network_peer(peer)
	
		get_tree().connect("network_peer_connected", self, "_player_connected")
		get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
		
		$ConnectingScreen.hide()
		
		$Buttons/HiddenDice.disabled = false
		$Buttons/Save.disabled = false
		
		_load()
		UPNPRequester.forward_port(int(Current.port))
		
		print("app started as server")
	elif Current.type == Current.Type.CLIENT:
		peer.create_client(Current.ip, int(Current.port))
		get_tree().set_network_peer(peer)
	
		get_tree().connect("connected_to_server", self, "_connected_ok")
		get_tree().connect("connection_failed", self, "_connected_fail")
		get_tree().connect("server_disconnected", self, "_server_disconnected")
		
		$Buttons/HiddenDice.disabled = true
		print("app started as client")
	else:
		print("invalid start type")
		
remote func _replace_logs(char_log: String, player_log: String):
	$CharacterLog/Content.bbcode_text = char_log
	$PlayerLog/Content.bbcode_text = player_log

# server related callback
func _player_connected(id):
	rpc_id(id, "_replace_logs", $CharacterLog/Content.bbcode_text, $PlayerLog/Content.bbcode_text)
	rpc_id(id, "_sync_notes", $NotepadDialog/Notes.text)
	Current.connected_players.append(id)
	
	$PlayerListDialog.hide()
	
	rpc("_connected_players_changed")
	_connected_players_changed()
	print("new player connceted: " + str(id))

func _player_disconnected(id):
	Current.connected_players.remove(Current.connected_players.find(id))
	
	if Current.connected_characters.has(id):
		_append_player_log("[b]守秘人[/b]", Current.connected_characters[id].name + "离开了游戏。")
		rpc("_append_player_log", "[b]守秘人[/b]", Current.connected_characters[id].name + "离开了游戏。")
		
	$PlayerListDialog.hide()
	
	rpc("_connected_players_changed")
	_connected_players_changed()
	
	print("player disconnceted: " + str(id))
# server callback end

remote func _append_player_character(id, character):
	# var temp_character = character.duplicate(true)
	var old_id = Current.append_to_connected_character(id, character.duplicate(true))
	if old_id != null:
		if Current.connected_players.find(old_id) != -1:
			# kick old player
			rpc_id(old_id, "_kicked")
	rpc("_append_player_log", "[b]守秘人[/b]", Current.connected_characters[id].name + "加入了游戏。")
	print("new character added to server: " + Current.connected_characters[id].name)

# client related callback
func _connected_ok():
	$ConnectingScreen.hide()
	rpc_id(1, "_append_player_character", peer.get_unique_id(), Current.character.duplicate(true))
	rpc_id(1, "_append_player_log", "[b]守秘人[/b]", Current.character.name + "加入了游戏。")
	print("successfully connected to server")

func _server_disconnected():
	$ConnectingScreen.show()
	$ConnectingScreen/HelpText.text = "服务器连接断开，请确认网络和地址之后返回主界面重试"
	$ConnectingScreen/ButtonCancel.text = "返回"
	
	$HiddenDiceDialog.hide()
	$NotepadDialog.hide()
	$PlayerListDialog.hide()
	
	$PrivateMsgDialog.hide()
	print("disconnected from server")

func _connected_fail():
	$ConnectingScreen/HelpText.text = "无法连接至服务器，请确认网络和地址之后返回主界面重试"
	$ConnectingScreen/ButtonCancel.text = "返回"
	peer.close_connection()
	print("unable to connect to server")
# client callback end

remote func _append_character_log(user: String, text: String):
	$CharacterLog/Content.bbcode_text = $CharacterLog/Content.bbcode_text + "[i]" + Current.get_time_stamp() + "[/i] " + user + "\n" + text + "\n"

func _on_SendAsCharacter_button_down():
	if $InputChat.text != "":
		match Current.type:
			Current.Type.CLIENT:
				_append_character_log("[b]" + Current.character.name + "[/b]", $InputChat.text)
				rpc("_append_character_log", "[b]" + Current.character.name + "[/b]", $InputChat.text)
			Current.Type.HOST:
				_append_character_log("[b]守秘人[/b]", $InputChat.text)
				rpc("_append_character_log", "[b]守秘人[/b]", $InputChat.text)
	$InputChat.text = ""
	$Buttons/SendAsCharacter.disabled = true
	$Buttons/SendAsPlayer.disabled = true

remote func _append_player_log(user: String, text: String):
	$PlayerLog/Content.bbcode_text = $PlayerLog/Content.bbcode_text + "[i]" + Current.get_time_stamp() + "[/i] " + user + "\n" + text + "\n"

func _on_SendAsPlayer_button_down():
	if $InputChat.text != "":
		match Current.type:
			Current.Type.CLIENT:
				_append_player_log("[b]" + Current.character.name + "[/b]", $InputChat.text)
				rpc("_append_player_log", "[b]" + Current.character.name + "[/b]", $InputChat.text)
			Current.Type.HOST:
				_append_player_log("[b]守秘人[/b]", $InputChat.text)
				rpc("_append_player_log", "[b]守秘人[/b]", $InputChat.text)
	$InputChat.text = ""
	$Buttons/SendAsCharacter.disabled = true
	$Buttons/SendAsPlayer.disabled = true

func _on_InputChat_text_changed(new_text):
	if new_text != "":
		if Validator.contain_bbcode(new_text):
			$Buttons/SendAsCharacter.disabled = true
			$Buttons/SendAsPlayer.disabled = true
			$InputChat.text = "请不要使用bbcode关键词"
		else:
			$Buttons/SendAsCharacter.disabled = false
			$Buttons/SendAsPlayer.disabled = false
	else:
		$Buttons/SendAsCharacter.disabled = true
		$Buttons/SendAsPlayer.disabled = true

func _on_D4_button_down():
	var result = str(Dice.d4())
	match Current.type:
		Current.Type.CLIENT:
			_append_player_log("[b]" + Current.character.name + "[/b]", "[b][color=#ff70cc]D4 = " + result + "[/color][/b]")
			rpc("_append_player_log", "[b]" + Current.character.name + "[/b]", "[b][color=#ff70cc]D4 = " + result + "[/color][/b]")
		Current.Type.HOST:
			_append_player_log("[b]守秘人[/b]", "[b][color=#ff70cc]D4 = " + result + "[/color][/b]")
			rpc("_append_player_log", "[b]守秘人[/b]", "[b][color=#ff70cc]D4 = " + result + "[/color][/b]")

func _on_D6_button_down():
	var result = str(Dice.d6())
	match Current.type:
		Current.Type.CLIENT:
			_append_player_log("[b]" + Current.character.name + "[/b]", "[b][color=#46c6e9]D6 = " + result + "[/color][/b]")
			rpc("_append_player_log", "[b]" + Current.character.name + "[/b]", "[b][color=#46c6e9]D6 = " + result + "[/color][/b]")
		Current.Type.HOST:
			_append_player_log("[b]守秘人[/b]", "[b][color=#46c6e9]D6 = " + result + "[/color][/b]")
			rpc("_append_player_log", "[b]守秘人[/b]", "[b][color=#46c6e9]D6 = " + result + "[/color][/b]")

func _on_D8_button_down():
	var result = str(Dice.d8())
	match Current.type:
		Current.Type.CLIENT:
			_append_player_log("[b]" + Current.character.name + "[/b]", "[b][color=#9999ff]D8 = " + result + "[/color][/b]")
			rpc("_append_player_log", "[b]" + Current.character.name + "[/b]", "[b][color=#9999ff]D8 = " + result + "[/color][/b]")
		Current.Type.HOST:
			_append_player_log("[b]守秘人[/b]", "[b][color=#9999ff]D8 = " + result + "[/color][/b]")
			rpc("_append_player_log", "[b]守秘人[/b]", "[b][color=#9999ff]D8 = " + result + "[/color][/b]")

func _on_D20_button_down():
	var result = str(Dice.d20())
	match Current.type:
		Current.Type.CLIENT:
			_append_player_log("[b]" + Current.character.name + "[/b]", "[b][color=#f6eb14]D20 = " + result + "[/color][/b]")
			rpc("_append_player_log", "[b]" + Current.character.name + "[/b]", "[b][color=#f6eb14]D20 = " + result + "[/color][/b]")
		Current.Type.HOST:
			_append_player_log("[b]守秘人[/b]", "[b][color=#f6eb14]D20 = " + result + "[/color][/b]")
			rpc("_append_player_log", "[b]守秘人[/b]", "[b][color=#f6eb14]D20 = " + result + "[/color][/b]")

func _on_D100_button_down():
	var result = str(Dice.d100())
	match Current.type:
		Current.Type.CLIENT:
			_append_player_log("[b]" + Current.character.name + "[/b]", "[b][color=#bef204]D100 = " + result + "[/color][/b]")
			rpc("_append_player_log", "[b]" + Current.character.name + "[/b]", "[b][color=#bef204]D100 = " + result + "[/color][/b]")
		Current.Type.HOST:
			_append_player_log("[b]守秘人[/b]", "[b][color=#bef204]D100 = " + result + "[/color][/b]")
			rpc("_append_player_log", "[b]守秘人[/b]", "[b][color=#bef204]D100 = " + result + "[/color][/b]")

remote func _sync_notes(notes):
	$NotepadDialog/Notes.text = notes
	
func _on_Notes_text_changed():
	rpc("_sync_notes", $NotepadDialog/Notes.text)

remote func _kicked():
	$ConnectingScreen.show()
	$ConnectingScreen/HelpText.text = "连接被服务器断开，请返回主界面"
	$ConnectingScreen/ButtonCancel.text = "返回"
	
	$HiddenDiceDialog.hide()
	$NotepadDialog.hide()
	$PlayerListDialog.hide()
	
	$PrivateMsgDialog.hide()
	print("kicked by server")
	peer.close_connection()

remote func _character_request_received(id):
	print("character request received: " + str(id))
	rpc_id(id, "_set_remote_character", Current.connected_characters[id].duplicate())

remote func _set_remote_character(character):
	$PlayerListDialog.display_remote_character(character)

func _on_Save_button_down():
	$SaveDialog.popup_centered()

func _on_DialogSave_button_down():
	Current.save_to_session($CharacterLog/Content.bbcode_text, $PlayerLog/Content.bbcode_text, $NotepadDialog/Notes.text, $SaveDialog/SaveName.text)
	$SaveDialog.hide()

remote func _connected_player_request_received(id):
	var connected = {}
	for id in Current.connected_players:
		connected[id] = Current.connected_characters[id].name
	rpc_id(id, "_set_connected_players", connected.duplicate())
	
remote func _set_connected_players(players):
	$PrivateMsgDialog.connected_player_received(players)

remote func _private_msg_received(msg):
	$PrivateMsgDialog.private_message_received(msg)

remote func _connected_players_changed():
	$PrivateMsgDialog.hide()
