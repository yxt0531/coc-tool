extends WindowDialog

var connected_players = {}
var connected_players_ids = []
var selected_player_id

func _on_PrivateMsg_button_down():
	match Current.type:
		Current.Type.CLIENT:
			_request_connected_character()
		Current.Type.HOST:
			populate_connected_player_list()
			popup_centered()

func populate_connected_player_list():
	$PlayerList.clear()
	match Current.type:
		Current.Type.HOST:
			for id in Current.connected_players:
				$PlayerList.add_item(Current.connected_characters[id].name)

func _request_connected_character():
	get_parent().rpc_id(1, "_connected_player_request_received", get_parent().peer.get_unique_id())

func connected_player_received(players):
	$PlayerList.clear()
	connected_players = players.duplicate()
	connected_players[1] = "守秘人"
	connected_players_ids.clear()
	selected_player_id = null
	match Current.type:
		Current.Type.CLIENT:
			for name in connected_players.values():
				$PlayerList.add_item(name)
			for id in connected_players.keys():
				connected_players_ids.append(id)
			popup_centered()

func private_message_received(msg):
	$Log.text = $Log.text + msg + "\n"
	get_parent().get_node("Buttons/PrivateMsg").text = "私聊 (!)"

func _on_PlayerList_item_selected(index):
	match Current.type:
		Current.Type.CLIENT:
			selected_player_id = connected_players_ids[index]
		Current.Type.HOST:
			selected_player_id = Current.connected_players[index]
	$ButtonSend.disabled = false
	print(selected_player_id)

func _on_PlayerList_nothing_selected():
	$ButtonSend.disabled = true

func _on_ButtonSend_button_down():
	if selected_player_id != null:
		match Current.type:
			Current.Type.CLIENT:
				private_message_received(Current.character.name + ": " + $InputMsg.text)
				get_parent().rpc_id(selected_player_id, "_private_msg_received", Current.character.name + ": " + $InputMsg.text)
				$InputMsg.text = ""
			Current.Type.HOST:
				private_message_received("守秘人: " + $InputMsg.text)
				get_parent().rpc_id(selected_player_id, "_private_msg_received", "守秘人: " + $InputMsg.text)
				$InputMsg.text = ""

func _on_PrivateMsgDialog_popup_hide():
	connected_players.clear()
	connected_players_ids.clear()
	selected_player_id = null
	$ButtonSend.disabled = true
	get_parent().get_node("Buttons/PrivateMsg").text = "私聊"

func _process(delta):
	if Input.is_action_just_pressed("character_send") or Input.is_action_just_pressed("player_send"):
		if not $ButtonSend.disabled and self.visible:
			_on_ButtonSend_button_down()
