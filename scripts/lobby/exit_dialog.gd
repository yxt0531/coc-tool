extends WindowDialog

func _on_Exit_button_down():
	popup_centered()

func _on_Yes_button_down():
	get_parent().peer.close_connection()
	get_tree().change_scene("res://scenes/main_menu.tscn")

func _on_No_button_down():
	hide()
