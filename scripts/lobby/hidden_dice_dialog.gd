extends WindowDialog

func _on_HD4_button_down():
	$Result.text = str(Dice.d4())

func _on_HD6_button_down():
	$Result.text = str(Dice.d6())

func _on_HD8_button_down():
	$Result.text = str(Dice.d8())

func _on_HD20_button_down():
	$Result.text = str(Dice.d20())

func _on_HD100_button_down():
	$Result.text = str(Dice.d100())

func _on_HiddenDice_button_down():
	popup_centered()
