extends ColorRect

func ready():
	$NonInputSummary.text = JSON.print(Temp.character, "	")

func _on_ButtonPrev_button_down():
	get_parent().show_step(get_parent().Step.SIX)

func _on_ButtonNext_button_down():
	CharacterDB.add_character(Temp.character.duplicate(true))
	get_tree().change_scene("res://scenes/main_menu.tscn")
