extends ColorRect

func _on_ButtonPrev_button_down():
	get_parent().show_step(get_parent().Step.THREE)

func _on_ButtonNext_button_down():
	Temp.character["bg_appearance"] = $LabelAppearance/InputAppearance.text
	Temp.character["bg_belief"] = $LabelBelief/InputBelief.text
	Temp.character["bg_person"] = $LabelPerson/InputPerson.text
	Temp.character["bg_place"] = $LabelPlace/InputPlace.text
	Temp.character["bg_treasure"] = $LabelTreasure/InputTreasure.text
	Temp.character["bg_personality"] = $LabelPersonality/InputPersonality.text
	get_parent().show_step(get_parent().Step.FIVE)
