extends ItemList

func populate_skill_lists():
	clear()
	for skill_name in get_parent().all_skills:
		add_item(skill_name)

func _on_SkillList_item_selected(index):
	get_parent().selected_index = index
	get_parent().get_node("ButtonAddToProSkillList").disabled = false
	get_parent().get_node("ButtonRemoveFromProSkillList").disabled = true
	get_parent().get_node("LabelSkillType").show_skill_type(get_item_text(index))
	get_parent().get_node("LabelSkillExtra").text = ""
	get_parent().get_node("InputProSkillPoint").show_skill_points(get_item_text(index))
	get_parent().get_node("InputProSkillPoint").editable = false
	get_parent().get_node("InputProSkillPoint/InputProSkillInterest").editable = true
