extends ItemList

func populate_skill_lists():
	clear()
	for skill_name in get_parent().selected_pro_skills:
		add_item(skill_name)
		
func _on_ProSkillList_item_selected(index):
	get_parent().selected_index = index
	
	if get_parent().locked_skills.find(get_item_text(index)) != -1:
		get_parent().get_node("ButtonAddToProSkillList").disabled = true
		if get_parent().get_node("CheckForceRemoval").pressed:
			get_parent().get_node("ButtonRemoveFromProSkillList").disabled = false
			get_parent().get_node("LabelSkillExtra").text = "固定本职技能"
		else:
			get_parent().get_node("ButtonRemoveFromProSkillList").disabled = true
			get_parent().get_node("LabelSkillExtra").text = "固定本职技能，无法移动"
	else:
		get_parent().get_node("ButtonAddToProSkillList").disabled = true
		get_parent().get_node("ButtonRemoveFromProSkillList").disabled = false
		get_parent().get_node("LabelSkillExtra").text = ""
		
	get_parent().get_node("LabelSkillType").show_skill_type(get_item_text(index))
	get_parent().get_node("InputProSkillPoint").show_skill_points(get_item_text(index))
	get_parent().get_node("InputProSkillPoint").editable = true
	get_parent().get_node("InputProSkillPoint/InputProSkillInterest").editable = true
