extends ColorRect

func _on_ButtonHide_button_down():
	self.hide()

func _on_ButtonShowSummary_button_down():
	update_summary()
	$Label.text = Character.get_pro_skills_help(Temp.profession) + "\n点击空白处返回"
	self.show()
	
func update_summary():
	$Summary.clear()
	for skill in Temp.skills.keys():
		var temp_string = skill + ": " + str(Temp.skills[skill].base) + ", " + str(Temp.skills[skill].pro) + ", " + str(Temp.skills[skill].interest) + ", 成功率：" + str(Temp.skills[skill].base + Temp.skills[skill].pro + Temp.skills[skill].interest) + "%"
		$Summary.add_item(temp_string)
