extends ColorRect

func _on_ButtonPrev_button_down():
	get_parent().show_step(get_parent().Step.FOUR)

func _on_ButtonNext_button_down():
	Temp.character["bg_scar"] = $LabelScar/InputScar.text
	Temp.character["bg_illness"] = $LabelIllness/InputIllness.text
	Temp.character["bg_extra"] = $LabelExtra/InputExtra.text
	get_parent().show_step(get_parent().Step.SIX)
