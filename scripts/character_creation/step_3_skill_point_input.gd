extends LineEdit

var current_skill

func show_skill_points(skill: String):
	current_skill = skill
	
	$NonInputBase.text = str(Temp.skills[skill].base)
	text = str(Temp.skills[skill].pro)
	$InputProSkillInterest.text = str(Temp.skills[skill].interest)
	
	$NonInputD1.text = str(int($NonInputBase.text) + int(text) + int($InputProSkillInterest.text))
	$NonInputD2.text = str((int($NonInputBase.text) + int(text) + int($InputProSkillInterest.text)) / 2)
	$NonInputD5.text = str((int($NonInputBase.text) + int(text) + int($InputProSkillInterest.text)) / 5)
	
	
func _on_InputProSkillPoint_text_changed(new_text):
	if Validator.is_number(new_text):
		Temp.skills[current_skill].pro = int(new_text)
	else:
		text = "0"
	
	$InputProSkillInterest.text = str(Temp.skills[current_skill].interest)
	$NonInputD1.text = str(int($NonInputBase.text) + int(text) + int($InputProSkillInterest.text))
	$NonInputD2.text = str((int($NonInputBase.text) + int(text) + int($InputProSkillInterest.text)) / 2)
	$NonInputD5.text = str((int($NonInputBase.text) + int(text) + int($InputProSkillInterest.text)) / 5)
	get_parent().update_help_text()

func _on_InputProSkillInterest_text_changed(new_text):
	if Validator.is_number(new_text):
		Temp.skills[current_skill].interest = int(new_text)
	else:
		$InputProSkillInterest.text = "0"
	
	text = str(Temp.skills[current_skill].pro)
	$NonInputD1.text = str(int($NonInputBase.text) + int(text) + int($InputProSkillInterest.text))
	$NonInputD2.text = str((int($NonInputBase.text) + int(text) + int($InputProSkillInterest.text)) / 2)
	$NonInputD5.text = str((int($NonInputBase.text) + int(text) + int($InputProSkillInterest.text)) / 5)
	get_parent().update_help_text()
