extends ColorRect

func _on_ButtonPrev_button_down():
	get_parent().show_step(get_parent().Step.FIVE)

func _on_ButtonNext_button_down():
	Temp.character["assets"] = $LabelAssets/InputAssets.text
	get_parent().get_node("Step7").ready()
	get_parent().show_step(get_parent().Step.SEVEN)
