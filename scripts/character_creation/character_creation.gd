extends Control

enum Step { ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN }

func _ready():
	show_step(Step.ONE) # uncomment this on release
	print("character creation scene ready")

func show_step(step):
	$Step1.hide()
	$Step2.hide()
	$Step3.hide()
	$Step4.hide()
	$Step5.hide()
	$Step6.hide()
	$Step7.hide()
	
	match step:
		Step.ONE:
			$Step1.show()
		Step.TWO:
			$Step2.show()
		Step.THREE:
			$Step3.show()
		Step.FOUR:
			$Step4.show()
		Step.FIVE:
			$Step5.show()
		Step.SIX:
			$Step6.show()
		Step.SEVEN:
			$Step7.show()
