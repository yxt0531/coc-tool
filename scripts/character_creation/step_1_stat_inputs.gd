extends LineEdit

func _on_text_changed(new_text):
	if Validator.is_number(new_text):
		$NonInputD2.text = str(int(new_text) / 2)
		$NonInputD5.text = str(int(new_text) / 5)
		get_parent().update_stat_total()
	elif new_text.to_upper().find("R") != -1:
		if self.name == "InputStr" or self.name == "InputCon" or self.name == "InputDex" or self.name == "InputApp" or self.name == "InputPow" or self.name == "InputLuck":
			# use 3d6x5 dice
			self.text = str(Dice.d6(3) * 5)
		else:
			self.text = str((Dice.d6(2) + 6) * 5)
		$NonInputD2.text = str(int(self.text) / 2)
		$NonInputD5.text = str(int(self.text) / 5)
		get_parent().update_stat_total()
	else:
		self.text = ""
