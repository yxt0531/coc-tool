extends ColorRect

var professions = {}

func _ready():
	$ButtonNext.disabled = true
	update_selected_profession()
	populate_profession_list()

func populate_profession_list():
	var list = $ProfessionList
	for profession in Character.professions.values():
		list.add_item(profession)

func update_selected_profession(index = -1):
	if index <= -1:
		$LabelHelpText.text = "请选择一个职业模板"
	else:
		$LabelHelpText.text = "选择的职业模板：" + Character.professions[index]

func _on_ButtonPrev_button_down():
	get_parent().show_step(get_parent().Step.ONE)

func _on_ButtonNext_button_down():
	Temp.character["profession"] = Character.professions[Temp.profession]
	get_parent().get_node("Step3").start()
	get_parent().show_step(get_parent().Step.THREE)

func _on_ProfessionList_item_selected(index):
	update_selected_profession(index)
	$ButtonNext.disabled = false
	Temp.profession = index

func _on_ProfessionList_nothing_selected():
	update_selected_profession()
	$ButtonNext.disabled = true
	Temp.profession = null

# quick debug test for all professions
#
#func _process(delta):
#	if Input.is_action_just_pressed("Debug"):
#		_debug()
#
#func _debug():
#	var debug_var = 0
#	while debug_var < 115:
#		update_selected_profession(debug_var)
#		Temp.profession = debug_var
#		_on_ButtonNext_button_down()
#
#		get_parent().get_node("Step3")._on_ButtonPrev_button_down()
#		debug_var += 1
#	print("passed")
