extends ColorRect

var base_character

func update_stat_total():
	var total: int = 0
	if $InputStr.text != "":
		total += int($InputStr.text)
	
	if $InputCon.text != "":
		total += int($InputCon.text)
	
	if $InputSiz.text != "":
		total += int($InputSiz.text)
	
	if $InputDex.text != "":
		total += int($InputDex.text)
	
	if $InputApp.text != "":
		total += int($InputApp.text)
	
	if $InputEdu.text != "":
		total += int($InputEdu.text)
	
	if $InputInt.text != "":
		total += int($InputInt.text)
	
	if $InputPow.text != "":
		total += int($InputPow.text)
	
	$NonInputStatTotal.text = str(total)
	
	_is_filled()
		
func update_derived_stat():
	# update db and build
	var str_siz = int($InputStr.text) + int($InputSiz.text)
	
	if str_siz >= 2 and str_siz <= 64:
		$DerivedStat/NonInputDB.text = "-2"
		$DerivedStat/NonInputBuild.text = "-2"
	elif str_siz >= 65 and str_siz <= 84:
		$DerivedStat/NonInputDB.text = "-1"
		$DerivedStat/NonInputBuild.text = "-1"
	elif str_siz >= 85 and str_siz <= 124:
		$DerivedStat/NonInputDB.text = "-0"
		$DerivedStat/NonInputBuild.text = "-0"
	elif str_siz >= 125 and str_siz <= 164:
		$DerivedStat/NonInputDB.text = "+1D4"
		$DerivedStat/NonInputBuild.text = "1"
	elif str_siz >= 165 and str_siz <= 204:
		$DerivedStat/NonInputDB.text = "+1D6"
		$DerivedStat/NonInputBuild.text = "2"
	elif str_siz >= 205 and str_siz <= 284:
		$DerivedStat/NonInputDB.text = "+2D6"
		$DerivedStat/NonInputBuild.text = "3"
	elif str_siz >= 285 and str_siz <= 364:
		$DerivedStat/NonInputDB.text = "+3D6"
		$DerivedStat/NonInputBuild.text = "4"
	elif str_siz >= 365 and str_siz <= 444:
		$DerivedStat/NonInputDB.text = "+4D6"
		$DerivedStat/NonInputBuild.text = "5"
	elif str_siz >= 445:
		$DerivedStat/NonInputDB.text = "+5D6"
		$DerivedStat/NonInputBuild.text = "6"

	# update mov
	var init_mov
	if int($InputDex.text) < int($InputSiz.text) and int($InputStr.text) < int($InputSiz.text):
		init_mov = 7
	elif int($InputDex.text) > int($InputSiz.text) and int($InputStr.text) > int($InputSiz.text):
		init_mov = 9
	else:
		init_mov = 8
		
	var final_mov
	if int($InputAge.text) >= 40 and int($InputAge.text) <= 49:
		final_mov = init_mov - 1
	elif int($InputAge.text) >= 50 and int($InputAge.text) <= 59:
		final_mov = init_mov - 2
	elif int($InputAge.text) >= 60 and int($InputAge.text) <= 69:
		final_mov = init_mov - 3
	elif int($InputAge.text) >= 70 and int($InputAge.text) <= 79:
		final_mov = init_mov - 4
	elif int($InputAge.text) >= 80:
		final_mov = init_mov - 5
	else:
		final_mov = init_mov
		
	$DerivedStat/NonInputMov.text = str(final_mov)

	# update hp
	$DerivedStat/NonInputHP.text = str((int($InputCon.text) + int($InputSiz.text)) / 10)
	
	# update san
	$DerivedStat/NonInputSAN.text = $InputPow.text
	
	# update mp
	$DerivedStat/NonInputMP.text = str(int($InputPow.text) / 5)
	
func _is_filled():
	if _is_stat_filled():
		$ButtonShowDerived.disabled = false
		if _is_info_filled():
			$ButtonNext.disabled = false
		else:
			$ButtonNext.disabled = true
	else:
		$ButtonShowDerived.disabled = true
		$ButtonNext.disabled = true
	
func _is_stat_filled():
	if $InputStr.text == "":
		return false
	elif $InputCon.text == "":
		return false
	elif $InputSiz.text == "":
		return false
	elif $InputDex.text == "":
		return false
	elif $InputApp.text == "":
		return false
	elif $InputEdu.text == "":
		return false
	elif $InputInt.text == "":
		return false
	elif $InputPow.text == "":
		return false
	elif $InputLuck.text == "":
		return false
	else:
		return true

func _is_info_filled():
	if $InputName.text == "" or $InputName.text == "守秘人":
		return false
	elif $InputPlayer.text == "":
		return false
#	elif $InputProfession.text == "":
#		return false
	elif $InputAge.text == "":
		return false
	elif $InputSex.text == "":
		return false
	elif $InputAddress.text == "":
		return false
	elif $InputOrigin.text == "":
		return false
	else:
		return true

func _update_base_character():
	# TODO add age multiplier
	if int($InputAge.text) >= 15 and int($InputAge.text) <= 19:
		pass
	elif int($InputAge.text) >= 20 and int($InputAge.text) <= 39:
		pass
	elif int($InputAge.text) >= 40 and int($InputAge.text) <= 49:
		pass
	elif int($InputAge.text) >= 50 and int($InputAge.text) <= 59:
		pass
	elif int($InputAge.text) >= 60 and int($InputAge.text) <= 69:
		pass
	elif int($InputAge.text) >= 70 and int($InputAge.text) <= 79:
		pass
	elif int($InputAge.text) >= 80 and int($InputAge.text) <= 89:
		pass
	# end TODO
	
	base_character = {
		"str": int($InputStr.text),
		"con": int($InputCon.text),
		"siz": int($InputSiz.text),
		"dex": int($InputDex.text),
		"app": int($InputApp.text),
		"edu": int($InputEdu.text),
		"int": int($InputInt.text),
		"pow": int($InputPow.text),
		"luck": int($InputLuck.text),
		"name": $InputName.text,
		"player": $InputPlayer.text,
		"job": $InputProfession.text,
		"age": int($InputAge.text),
		"sex": $InputSex.text,
		"address": $InputAddress.text,
		"origin": $InputOrigin.text
	}
	
	Temp.character = base_character
	print("temp character updated")
	
func _on_InputAge_text_changed(new_text):
	if not Validator.is_number(new_text):
		$InputAge.text = ""
	elif int(new_text) < 0:
		$InputAge.text = ""
		
	_is_filled()

func _on_SliderEra_value_changed(value):
	if value <= 0 and value < 1:
		$SliderEra/LabelEra.text = "时代背景-1890s"
	elif value <= 1 and value < 2:
		$SliderEra/LabelEra.text = "时代背景-1920s"
	else:
		$SliderEra/LabelEra.text = "现代"
		
func _on_ButtonRandomizeAll_button_down():
	$InputStr._on_text_changed("R")
	$InputCon._on_text_changed("R")
	$InputSiz._on_text_changed("R")
	$InputDex._on_text_changed("R")
	$InputApp._on_text_changed("R")
	$InputEdu._on_text_changed("R")
	$InputInt._on_text_changed("R")
	$InputPow._on_text_changed("R")
	$InputLuck._on_text_changed("R")

func _on_ButtonShowDerived_button_down():
	update_derived_stat()
	$DerivedStat.show()

func _on_ButtonPrev_button_down():
	get_tree().change_scene("res://scenes/main_menu.tscn")

func _on_ButtonNext_button_down():
	get_parent().show_step(get_parent().Step.TWO)
	_update_base_character()
