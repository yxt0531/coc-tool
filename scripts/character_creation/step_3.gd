extends ColorRect

var all_skills = []
var selected_pro_skills = []
var selected_index
var locked_skills = []

var skills_art_max_remaining
var skills_science_max_remaining
var skills_combat_max_remaining
var skills_shooting_max_remaining
var skills_special_max_remaining
var skills_social_max_remaining
var skills_any_max_remaining

func _ready():
	pass

func _on_ButtonPrev_button_down():
	get_parent().show_step(get_parent().Step.TWO)

func _on_ButtonNext_button_down():
	Temp.character["skills"] = Temp.skills
	get_parent().show_step(get_parent().Step.FOUR)

func start():
	$LabelSkillType.text = ""
	$LabelSkillExtra.text = ""
	
	Temp.skills_art_custom_remaining = 0
	Temp.skills_science_custom_remaining = 0
	Temp.skills_combat_custom_remaining = 0
	Temp.skills_shooting_custom_remaining = 0
	Temp.skills_special_custom_remaining = 0
	Temp.skills_social_custom_remaining = 0
	Temp.skills_any_custom_remaining = 0
	
	all_skills.clear()
	selected_pro_skills.clear()
	locked_skills.clear()
	
	for skill_name in Character.skills.keys():
		all_skills.append(skill_name)
	for skill_name in Character.skills_art.keys():
		all_skills.append(skill_name)
	for skill_name in Character.skills_science.keys():
		all_skills.append(skill_name)
	for skill_name in Character.skills_combat.keys():
		all_skills.append(skill_name)
	for skill_name in Character.skills_shooting.keys():
		all_skills.append(skill_name)
	for skill_name in Character.skills_special.keys():
		all_skills.append(skill_name)
	for skill_name in Character.skills_social.keys():
		all_skills.append(skill_name)
	
	# trim skills by era
	if get_parent().get_node("Step1/SliderEra").value < 2:
		all_skills.remove(all_skills.find("计算机使用"))
		all_skills.remove(all_skills.find("电子学"))
	
	for skill in all_skills:
		# populate temp skills with extra attributes
		
		Temp.skills[skill] = {
			"base": Character.get_skill_base_point(skill),
			"pro": 0,
			"interest": 0
		}
	
	_get_character_skillpoints()
	update_help_text()
	_add_pro_skills()
	_update_max_pro_skills_remainings()
	$LabelRemainingProSkills.show_remaining_pro_skills()
	
	get_node("SkillList").populate_skill_lists()
	get_node("ProSkillList").populate_skill_lists()

func _get_character_skillpoints():
	Temp.skillpoint = Character.get_pro_skillpoints(Temp.profession, Temp.character)
	print("character skillpoints: " + str(Temp.skillpoint))

func update_help_text():
	var spent_pro_skill_points = 0
	var spent_interest_points = 0
	
	for skills in Temp.skills.values():
		spent_pro_skill_points += skills.pro
		spent_interest_points += skills.interest
	
	$LabelHelpText.text = "将" + str(Temp.skillpoint - spent_pro_skill_points) + "点本职技能点数分配给本职技能和信用评级，和" + str(Temp.interest_point - spent_interest_points) + "点兴趣技能点数分配给任意技能。"
	if Temp.skillpoint - spent_pro_skill_points < 0 or Temp.interest_point - spent_interest_points < 0:
		$ButtonNext.disabled = true
	else:
		$ButtonNext.disabled = false

func _add_pro_skills():
	var skill_list = Character.get_pro_skills(Temp.profession)
	# ["艺术与手艺", "科学", "格斗", "射击", "非常规技能", "社交", "任意"]
	for skill in skill_list:
		match skill:
			"艺术与手艺":
				Temp.skills_art_custom_remaining += 1
			"科学":
				Temp.skills_science_custom_remaining += 1
			"格斗":
				Temp.skills_combat_custom_remaining += 1
			"射击":
				Temp.skills_shooting_custom_remaining += 1
			"非常规技能":
				Temp.skills_special_custom_remaining += 1
			"社交":
				Temp.skills_social_custom_remaining += 1
			"任意":
				Temp.skills_any_custom_remaining += 1
			_:
				# trim skills by era
				if get_parent().get_node("Step1/SliderEra").value < 2:
					if skill == "计算机使用" or skill == "电子学":
						pass
					else:
						selected_pro_skills.append(all_skills[all_skills.find(skill)])
						locked_skills.append(all_skills[all_skills.find(skill)])
						all_skills.remove(all_skills.find(skill))
				else:
					# modern era
					selected_pro_skills.append(all_skills[all_skills.find(skill)])
					locked_skills.append(all_skills[all_skills.find(skill)])
					all_skills.remove(all_skills.find(skill))

func _update_max_pro_skills_remainings():
	skills_art_max_remaining = Temp.skills_art_custom_remaining
	skills_science_max_remaining = Temp.skills_science_custom_remaining 
	skills_combat_max_remaining = Temp.skills_combat_custom_remaining
	skills_shooting_max_remaining = Temp.skills_shooting_custom_remaining
	skills_special_max_remaining = Temp.skills_special_custom_remaining
	skills_social_max_remaining = Temp.skills_social_custom_remaining
	skills_any_max_remaining = Temp.skills_any_custom_remaining

func _on_ButtonAddToProSkillList_button_down():
	# add skill from skill list to pro skill list
	if _adjust_remaining_skills(all_skills[selected_index], true):
		$LabelRemainingProSkills.show_remaining_pro_skills()
		selected_pro_skills.append(all_skills[selected_index])
		all_skills.remove(selected_index)
		
		$SkillList.populate_skill_lists()
		$ProSkillList.populate_skill_lists()
		get_node("ButtonAddToProSkillList").disabled = true
		get_node("ButtonRemoveFromProSkillList").disabled = true
	else:
		$LabelSkillExtra.text = "无剩余可用技能，无法添加"
	

func _on_ButtonRemoveFromProSkillList_button_down():
	if _adjust_remaining_skills(selected_pro_skills[selected_index], false):
		$LabelRemainingProSkills.show_remaining_pro_skills()
		var skill = selected_pro_skills[selected_index]
		all_skills.append(selected_pro_skills[selected_index])
		selected_pro_skills.remove(selected_index)
		
		$SkillList.populate_skill_lists()
		$ProSkillList.populate_skill_lists()
		get_node("ButtonAddToProSkillList").disabled = true
		get_node("ButtonRemoveFromProSkillList").disabled = true
	
func _adjust_remaining_skills(skill: String, is_adding: bool):
	if is_adding:
		match Character.get_skill_type(skill):
			"普通":
				if Temp.skills_any_custom_remaining > 0:
					Temp.skills_any_custom_remaining -= 1
					return true
				else:
					return false
			"艺术与手艺":
				if Temp.skills_art_custom_remaining > 0:
					Temp.skills_art_custom_remaining -= 1
					return true
				else:
					if Temp.skills_any_custom_remaining > 0:
						Temp.skills_any_custom_remaining -= 1
						return true
					else:
						return false
			"科学":
				if Temp.skills_science_custom_remaining > 0:
					Temp.skills_science_custom_remaining -= 1
					return true
				else:
					if Temp.skills_any_custom_remaining > 0:
						Temp.skills_any_custom_remaining -= 1
						return true
					else:
						return false
			"格斗":
				if Temp.skills_combat_custom_remaining > 0:
					Temp.skills_combat_custom_remaining -= 1
					return true
				else:
					if Temp.skills_any_custom_remaining > 0:
						Temp.skills_any_custom_remaining -= 1
						return true
					else:
						return false
			"射击":
				if Temp.skills_shooting_custom_remaining > 0:
					Temp.skills_shooting_custom_remaining -= 1
					return true
				else:
					if Temp.skills_any_custom_remaining > 0:
						Temp.skills_any_custom_remaining -= 1
						return true
					else:
						return false
			"非常规技能":
				if Temp.skills_special_custom_remaining > 0:
					Temp.skills_special_custom_remaining -= 1
					return true
				else:
					if Temp.skills_any_custom_remaining > 0:
						Temp.skills_any_custom_remaining -= 1
						return true
					else:
						return false
			"社交":
				if Temp.skills_social_custom_remaining > 0:
					Temp.skills_social_custom_remaining -= 1
					return true
				else:
					if Temp.skills_any_custom_remaining > 0:
						Temp.skills_any_custom_remaining -= 1
						return true
					else:
						return false
	else:
		match Character.get_skill_type(skill):
			"普通":
				Temp.skills_any_custom_remaining += 1
			"艺术与手艺":
				if Temp.skills_art_custom_remaining < skills_art_max_remaining:
					Temp.skills_art_custom_remaining += 1
				else:
					Temp.skills_any_custom_remaining += 1
			"科学":
				if Temp.skills_science_custom_remaining < skills_science_max_remaining:
					Temp.skills_science_custom_remaining += 1
				else:
					Temp.skills_any_custom_remaining += 1
			"格斗":
				if Temp.skills_combat_custom_remaining < skills_combat_max_remaining:
					Temp.skills_combat_custom_remaining += 1
				else:
					Temp.skills_any_custom_remaining += 1
			"射击":
				if Temp.skills_shooting_custom_remaining < skills_shooting_max_remaining:
					Temp.skills_shooting_custom_remaining += 1
				else:
					Temp.skills_any_custom_remaining += 1
			"非常规技能":
				if Temp.skills_special_custom_remaining < skills_special_max_remaining:
					Temp.skills_special_custom_remaining += 1
				else:
					Temp.skills_any_custom_remaining += 1
			"社交":
				if Temp.skills_social_custom_remaining < skills_social_max_remaining:
					Temp.skills_social_custom_remaining += 1
				else:
					Temp.skills_any_custom_remaining += 1
		return true
	return false # safe guard
	
func _on_CheckForceRemoval_toggled(button_pressed):
	if not button_pressed:
		$ButtonRemoveFromProSkillList.disabled = true
# f*** this page, it took FOREVER!!!



