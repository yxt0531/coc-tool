extends Node

var characters = {}

var save_file

func _init():
	save_file = File.new()
	_load()

func _exit_tree():
	_save()

func add_character(new_character):
	randomize()
	var id = randi()
	while not characters.has(id):
		characters[id] = new_character.duplicate(true)
		print("new character added with id: " + str(id))
	_save()

func replace_character(id, new_character):
	characters[id] = new_character.duplicate(true)
	print("new character replaced with id: " + str(id))
	_save()

func delete_character(id):
	characters.erase(id)
	_save()

func clear():
	characters.clear()
	_save()
	print("character database cleared")

func _save():
	save_file.open(GlobalPath.CHARACTER_DB, File.WRITE)
	save_file.store_string(to_json(characters))
	save_file.close()
	print("character database saved")
	
func _load():
	print("loading characters from file")
	if not save_file.file_exists(GlobalPath.CHARACTER_DB):
		print("no character database found")
		return
	else:
		save_file.open(GlobalPath.CHARACTER_DB, File.READ)
		characters = parse_json(save_file.get_line())
		save_file.close()
		print("character database loaded")

