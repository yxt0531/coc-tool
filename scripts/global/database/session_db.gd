extends Node

var sessions = {}
var save_file

func _init():
	save_file = File.new()
	_load()

func _exit_tree():
	_save()

func add_session(session):
	randomize()
	var id = randi()
	while not sessions.has(id):
		sessions[id] = session.duplicate(true)
		print("new session added with id: " + str(id))
	_save()

func delete_session(id):
	sessions.erase(id)
	_save()

func clear():
	sessions.clear()
	_save()
	print("sessions database cleared")

func _save():
	save_file.open(GlobalPath.SESSION_DB, File.WRITE)
	save_file.store_string(to_json(sessions))
	save_file.close()
	print("session database saved")
	
func _load():
	print("loading sessions from file")
	if not save_file.file_exists(GlobalPath.SESSION_DB):
		print("no session database found")
		return
	else:
		save_file.open(GlobalPath.SESSION_DB, File.READ)
		sessions = parse_json(save_file.get_line())
		save_file.close()
		print("session database loaded")

