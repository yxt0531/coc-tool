extends Node

var professions = {}
var professions_credit_min = {}
var professions_credit_max = {}
var skills = {}
var skills_art = {}
var skills_science = {}
var skills_combat = {}
var skills_shooting = {}
var skills_special = {}
var skills_social = {}

var skills_info_category = ["艺术与手艺", "科学", "格斗", "射击", "非常规技能", "社交", "任意"]

func _init():
	_populate_professions()
	_populate_professions_creadit_min()
	_populate_professions_creadit_max()
	_populate_skills()
	
func _populate_professions():
	professions[0] = "自定义职业"
	professions[1] = "会计师"
	professions[2] = "杂技演员"
	professions[3] = "演员-戏剧演员"
	professions[4] = "演员-电影演员"
	professions[5] = "中介调查员"
	professions[6] = "精神病医生(古典)"
	professions[7] = "动物训练师"
	professions[8] = "文物学家(原作向)"
	professions[9] = "古董商"
	professions[10] = "考古学家(原作向)"
	professions[11] = "建筑师"
	professions[12] = "艺术家"
	professions[13] = "精神病院护工"
	professions[14] = "运动员"
	professions[15] = "作家(原作向)"
	professions[16] = "酒保"
	professions[17] = "猎人"
	professions[18] = "书商"
	professions[19] = "赏金猎人"
	professions[20] = "拳击手、摔跤手"
	professions[21] = "管家、男仆、女仆"
	professions[22] = "神职人员"
	professions[23] = "程序员、电子工程师"
	professions[24] = "黑客/骇客"
	professions[25] = "牛仔"
	professions[26] = "工匠"
	professions[27] = "罪犯-刺客"
	professions[28] = "罪犯-银行劫匪"
	professions[29] = "罪犯-打手、暴徒"
	professions[30] = "罪犯-窃贼"
	professions[31] = "罪犯-欺诈师"
	professions[32] = "罪犯-独行罪犯"
	professions[33] = "罪犯-女飞贼(古典)"
	professions[34] = "罪犯-赃物贩子"
	professions[35] = "罪犯-赝造者"
	professions[36] = "罪犯-走私者"
	professions[37] = "罪犯-混混"
	professions[38] = "教团首领"
	professions[39] = "除魅师(现代)"
	professions[40] = "设计师"
	professions[41] = "业余艺术爱好者(原作向)"
	professions[42] = "潜水员"
	professions[43] = "医生(原作向)"
	professions[44] = "流浪者"
	professions[45] = "司机-私人司机"
	professions[46] = "司机-司机"
	professions[47] = "司机-出租车司机"
	professions[48] = "编辑"
	professions[49] = "政府官员"
	professions[50] = "工程师"
	professions[51] = "艺人"
	professions[52] = "探险家(古典)"
	professions[53] = "农民"
	professions[54] = "司法人员"
	professions[55] = "消防员"
	professions[56] = "驻外记者"
	professions[57] = "法医"
	professions[58] = "赌徒"
	professions[59] = "黑帮-黑帮老大"
	professions[60] = "黑帮-马仔"
	professions[61] = "绅士、淑女"
	professions[62] = "游民"
	professions[63] = "勤杂护工"
	professions[64] = "记者(原作向)-调查记者"
	professions[65] = "记者(原作向)-通讯记者"
	professions[66] = "法官"
	professions[67] = "实验室助理"
	professions[68] = "工人-非熟练工人"
	professions[69] = "工人-伐木工"
	professions[70] = "工人-矿工"
	professions[71] = "律师"
	professions[72] = "图书馆管理员(原作向)"
	professions[73] = "技师"
	professions[74] = "军官"
	professions[75] = "传教士"
	professions[76] = "登山家"
	professions[77] = "博物馆管理员"
	professions[78] = "音乐家"
	professions[79] = "护士"
	professions[80] = "神秘学家"
	professions[81] = "旅行家"
	professions[82] = "超心理学家"
	professions[83] = "药剂师"
	professions[84] = "摄影师-摄影师"
	professions[85] = "摄影师-摄影记者"
	professions[86] = "飞行员-飞行员"
	professions[87] = "飞行员-特技飞行员(古典)"
	professions[88] = "警方(原作向)-警探"
	professions[89] = "警方(原作向)-巡警"
	professions[90] = "私家侦探"
	professions[91] = "教授(原作向)"
	professions[92] = "淘金客"
	professions[93] = "性工作者"
	professions[94] = "精神病学家"
	professions[95] = "心理学家、精神分析学家"
	professions[96] = "研究员"
	professions[97] = "海员-军舰海员"
	professions[98] = "海员-民船海员"
	professions[99] = "推销员"
	professions[100] = "科学家"
	professions[101] = "秘书"
	professions[102] = "店老板"
	professions[103] = "士兵、海军陆战队士兵"
	professions[104] = "间谍"
	professions[105] = "学生、实习生"
	professions[106] = "替身演员"
	professions[107] = "部落成员"
	professions[108] = "殡葬师"
	professions[109] = "工会活动家"
	professions[110] = "服务生"
	professions[111] = "白领工人-职员、主管"
	professions[112] = "白领工人-中高层管理人员"
	professions[113] = "狂热者"
	professions[114] = "饲养员"
	print("character professions populated")

func _populate_professions_creadit_min():
	professions_credit_min[0] = 0
	professions_credit_min[1] = 30
	professions_credit_min[2] = 9
	professions_credit_min[3] = 9
	professions_credit_min[4] = 20
	professions_credit_min[5] = 20
	professions_credit_min[6] = 10
	professions_credit_min[7] = 10
	professions_credit_min[8] = 30
	professions_credit_min[9] = 30
	professions_credit_min[10] = 10
	professions_credit_min[11] = 30
	professions_credit_min[12] = 9
	professions_credit_min[13] = 8
	professions_credit_min[14] = 9
	professions_credit_min[15] = 9
	professions_credit_min[16] = 8
	professions_credit_min[17] = 20
	professions_credit_min[18] = 20
	professions_credit_min[19] = 9
	professions_credit_min[20] = 9
	professions_credit_min[21] = 9
	professions_credit_min[22] = 9
	professions_credit_min[23] = 10
	professions_credit_min[24] = 10
	professions_credit_min[25] = 9
	professions_credit_min[26] = 10
	professions_credit_min[27] = 30
	professions_credit_min[28] = 5
	professions_credit_min[29] = 5
	professions_credit_min[30] = 5
	professions_credit_min[31] = 10
	professions_credit_min[32] = 5
	professions_credit_min[33] = 10
	professions_credit_min[34] = 20
	professions_credit_min[35] = 20
	professions_credit_min[36] = 20
	professions_credit_min[37] = 3
	professions_credit_min[38] = 30
	professions_credit_min[39] = 20
	professions_credit_min[40] = 20
	professions_credit_min[41] = 50
	professions_credit_min[42] = 9
	professions_credit_min[43] = 30
	professions_credit_min[44] = 0
	professions_credit_min[45] = 10
	professions_credit_min[46] = 9
	professions_credit_min[47] = 9
	professions_credit_min[48] = 10
	professions_credit_min[49] = 50
	professions_credit_min[50] = 30
	professions_credit_min[51] = 9
	professions_credit_min[52] = 55
	professions_credit_min[53] = 9
	professions_credit_min[54] = 20
	professions_credit_min[55] = 9
	professions_credit_min[56] = 10
	professions_credit_min[57] = 40
	professions_credit_min[58] = 8
	professions_credit_min[59] = 60
	professions_credit_min[60] = 9
	professions_credit_min[61] = 40
	professions_credit_min[62] = 0
	professions_credit_min[63] = 6
	professions_credit_min[64] = 9
	professions_credit_min[65] = 9
	professions_credit_min[66] = 50
	professions_credit_min[67] = 10
	professions_credit_min[68] = 9
	professions_credit_min[69] = 9
	professions_credit_min[70] = 9
	professions_credit_min[71] = 30
	professions_credit_min[72] = 9
	professions_credit_min[73] = 9
	professions_credit_min[74] = 20
	professions_credit_min[75] = 0
	professions_credit_min[76] = 30
	professions_credit_min[77] = 10
	professions_credit_min[78] = 9
	professions_credit_min[79] = 9
	professions_credit_min[80] = 9
	professions_credit_min[81] = 5
	professions_credit_min[82] = 9
	professions_credit_min[83] = 35
	professions_credit_min[84] = 9
	professions_credit_min[85] = 10
	professions_credit_min[86] = 20
	professions_credit_min[87] = 30
	professions_credit_min[88] = 20
	professions_credit_min[89] = 9
	professions_credit_min[90] = 9
	professions_credit_min[91] = 20
	professions_credit_min[92] = 0
	professions_credit_min[93] = 5
	professions_credit_min[94] = 30
	professions_credit_min[95] = 10
	professions_credit_min[96] = 9
	professions_credit_min[97] = 9
	professions_credit_min[98] = 20
	professions_credit_min[99] = 9
	professions_credit_min[100] = 9
	professions_credit_min[101] = 9
	professions_credit_min[102] = 20
	professions_credit_min[103] = 9
	professions_credit_min[104] = 20
	professions_credit_min[105] = 5
	professions_credit_min[106] = 10
	professions_credit_min[107] = 0
	professions_credit_min[108] = 20
	professions_credit_min[109] = 5
	professions_credit_min[110] = 9
	professions_credit_min[111] = 9
	professions_credit_min[112] = 20
	professions_credit_min[113] = 0
	professions_credit_min[114] = 9
	print("character min credits populated")

func _populate_professions_creadit_max():
	professions_credit_max[0] = 99
	professions_credit_max[1] = 70
	professions_credit_max[2] = 20
	professions_credit_max[3] = 40
	professions_credit_max[4] = 90
	professions_credit_max[5] = 45
	professions_credit_max[6] = 60
	professions_credit_max[7] = 40
	professions_credit_max[8] = 70
	professions_credit_max[9] = 50
	professions_credit_max[10] = 40
	professions_credit_max[11] = 70
	professions_credit_max[12] = 50
	professions_credit_max[13] = 20
	professions_credit_max[14] = 70
	professions_credit_max[15] = 30
	professions_credit_max[16] = 25
	professions_credit_max[17] = 50
	professions_credit_max[18] = 40
	professions_credit_max[19] = 30
	professions_credit_max[20] = 60
	professions_credit_max[21] = 40
	professions_credit_max[22] = 60
	professions_credit_max[23] = 60
	professions_credit_max[24] = 70
	professions_credit_max[25] = 20
	professions_credit_max[26] = 40
	professions_credit_max[27] = 60
	professions_credit_max[28] = 75
	professions_credit_max[29] = 30
	professions_credit_max[30] = 40
	professions_credit_max[31] = 65
	professions_credit_max[32] = 65
	professions_credit_max[33] = 80
	professions_credit_max[34] = 40
	professions_credit_max[35] = 60
	professions_credit_max[36] = 60
	professions_credit_max[37] = 10
	professions_credit_max[38] = 60
	professions_credit_max[39] = 50
	professions_credit_max[40] = 60
	professions_credit_max[41] = 99
	professions_credit_max[42] = 30
	professions_credit_max[43] = 80
	professions_credit_max[44] = 5
	professions_credit_max[45] = 40
	professions_credit_max[46] = 20
	professions_credit_max[47] = 30
	professions_credit_max[48] = 30
	professions_credit_max[49] = 90
	professions_credit_max[50] = 60
	professions_credit_max[51] = 70
	professions_credit_max[52] = 80
	professions_credit_max[53] = 30
	professions_credit_max[54] = 40
	professions_credit_max[55] = 30
	professions_credit_max[56] = 40
	professions_credit_max[57] = 60
	professions_credit_max[58] = 50
	professions_credit_max[59] = 95
	professions_credit_max[60] = 20
	professions_credit_max[61] = 90
	professions_credit_max[62] = 5
	professions_credit_max[63] = 15
	professions_credit_max[64] = 30
	professions_credit_max[65] = 30
	professions_credit_max[66] = 80
	professions_credit_max[67] = 30
	professions_credit_max[68] = 30
	professions_credit_max[69] = 30
	professions_credit_max[70] = 30
	professions_credit_max[71] = 80
	professions_credit_max[72] = 35
	professions_credit_max[73] = 40
	professions_credit_max[74] = 70
	professions_credit_max[75] = 30
	professions_credit_max[76] = 60
	professions_credit_max[77] = 30
	professions_credit_max[78] = 30
	professions_credit_max[79] = 30
	professions_credit_max[80] = 65
	professions_credit_max[81] = 20
	professions_credit_max[82] = 30
	professions_credit_max[83] = 75
	professions_credit_max[84] = 30
	professions_credit_max[85] = 30
	professions_credit_max[86] = 70
	professions_credit_max[87] = 60
	professions_credit_max[88] = 50
	professions_credit_max[89] = 30
	professions_credit_max[90] = 30
	professions_credit_max[91] = 70
	professions_credit_max[92] = 10
	professions_credit_max[93] = 50
	professions_credit_max[94] = 80
	professions_credit_max[95] = 40
	professions_credit_max[96] = 30
	professions_credit_max[97] = 30
	professions_credit_max[98] = 40
	professions_credit_max[99] = 40
	professions_credit_max[100] = 50
	professions_credit_max[101] = 30
	professions_credit_max[102] = 40
	professions_credit_max[103] = 30
	professions_credit_max[104] = 60
	professions_credit_max[105] = 10
	professions_credit_max[106] = 50
	professions_credit_max[107] = 15
	professions_credit_max[108] = 40
	professions_credit_max[109] = 50
	professions_credit_max[110] = 20
	professions_credit_max[111] = 20
	professions_credit_max[112] = 80
	professions_credit_max[113] = 30
	professions_credit_max[114] = 40
	print("character max credits populated")
	
func _populate_skills():
	skills["会计"] = 5
	skills["人类学"] = 1
	skills["估价"] = 5
	skills["考古学"] = 1
	skills["攀爬"] = 20
	skills["计算机使用"] = 5
	skills["信用评级"] = 0
	skills["克苏鲁神话"] = 0
	skills["乔装"] = 5
	skills["闪避"] = 25
	skills["汽车驾驶"] = 20
	skills["电气维修"] = 10
	skills["电子学"] = 1
	skills["急救"] = 30
	skills["历史"] = 5
	skills["跳跃"] = 20
	skills["母语"] = 80
	skills["外语"] = 1
	skills["法律"] = 5
	skills["图书馆使用"] = 20
	skills["聆听"] = 20
	skills["锁匠"] = 1
	skills["机械维修"] = 10
	skills["医学"] = 1
	skills["博物学"] = 10
	skills["领航"] = 10
	skills["神秘学"] = 5
	skills["操作重型机械"] = 1
	skills["驾驶"] = 1
	skills["精神分析"] = 1
	skills["心理学"] = 10
	skills["骑术"] = 5
	skills["妙手"] = 10
	skills["侦查"] = 25
	skills["潜行"] = 20
	skills["生存"] = 10
	skills["游泳"] = 20
	skills["投掷"] = 20
	skills["追踪"] = 10
	skills["潜水"] = 1
	
	skills_art["表演"] = 5
	skills_art["美术"] = 5
	skills_art["摄影"] = 5
	skills_art["伪造文书"] = 5
	skills_art["写作"] = 5
	skills_art["书法"] = 5
	skills_art["音乐"] = 5
	skills_art["厨艺"] = 5
	skills_art["理发"] = 5
	skills_art["木匠"] = 5
	skills_art["舞蹈"] = 5
	skills_art["莫里斯舞蹈"] = 5
	skills_art["歌剧演唱"] = 5
	skills_art["粉刷/油漆工"] = 5
	skills_art["制陶"] = 5
	skills_art["雕塑"] = 5
	skills_art["技术制图"] = 5
	skills_art["文学"] = 5
	skills_art["耕作"] = 5
	skills_art["打字"] = 5
	skills_art["速记"] = 5

	skills_science["地质学"] = 1
	skills_science["化学"] = 1
	skills_science["生物学"] = 1
	skills_science["数学"] = 10
	skills_science["天文学"] = 1
	skills_science["物理学"] = 1
	skills_science["药学"] = 1
	skills_science["植物学"] = 1
	skills_science["动物学"] = 1
	skills_science["密码学"] = 1
	skills_science["工程学"] = 1
	skills_science["气象学"] = 1
	skills_science["司法科学"] = 1

	skills_combat["鞭子"] = 5
	skills_combat["电锯"] = 10
	skills_combat["斗殴"] = 25
	skills_combat["斧"] = 15
	skills_combat["剑"] = 20
	skills_combat["绞索"] = 15
	skills_combat["链枷"] = 10
	skills_combat["矛"] = 20
	
	skills_shooting["步枪/霰弹枪"] = 25
	skills_shooting["冲锋枪"] = 15
	skills_shooting["弓"] = 15
	skills_shooting["火焰喷射器"] = 10
	skills_shooting["机枪"] = 10
	skills_shooting["手枪"] = 20
	skills_shooting["重武器"] = 10
	
	skills_combat["爆破"] = 1
	skills_combat["催眠"] = 1
	skills_combat["读唇"] = 1
	skills_combat["炮术"] = 1
	skills_combat["潜水"] = 1
	skills_combat["驯兽"] = 5
	
	skills_social["魅惑"] = 15
	skills_social["话术"] = 5
	skills_social["恐吓"] = 15
	skills_social["说服"] = 10
	
	print("skills populated")
	
func get_pro_skillpoints(prof_index: int, base_character):
	var base_edu_points = base_character.edu * 2
	match prof_index:
		0:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2), (base_character.app * 2), (base_character.pow * 2), (base_character.int * 2)])
		1:
			return base_edu_points + (base_character.edu * 2)
		2:
			return base_edu_points + (base_character.dex * 2)
		3:
			return base_edu_points + (base_character.app * 2)
		4:
			return base_edu_points + (base_character.app * 2)
		5:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		6:
			return base_edu_points + (base_character.edu * 2)
		7:
			return base_edu_points + Math.get_max([(base_character.app * 2), (base_character.pow * 2)])
		8:
			return base_edu_points + (base_character.edu * 2)
		9:
			return base_edu_points + (base_character.edu * 2)
		10:
			return base_edu_points + (base_character.edu * 2)
		11:
			return base_edu_points + (base_character.edu * 2)
		12:
			return base_edu_points + Math.get_max([(base_character.dex * 2), (base_character.pow * 2)])
		13:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		14:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		15:
			return base_edu_points + (base_character.edu * 2)
		16:
			return base_edu_points + (base_character.app * 2)
		17:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		18:
			return base_edu_points + (base_character.edu * 2)
		19:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		20:
			return base_edu_points + (base_character.str * 2)
		21:
			return base_edu_points + (base_character.edu * 2)
		22:
			return base_edu_points + (base_character.edu * 2)
		23:
			return base_edu_points + (base_character.edu * 2)
		24:
			return base_edu_points + (base_character.edu * 2)
		25:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		26:
			return base_edu_points + (base_character.dex * 2)
		27:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		28:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		29:
			return base_edu_points + (base_character.str * 2)
		30:
			return base_edu_points + (base_character.dex * 2)
		31:
			return base_edu_points + (base_character.app * 2)
		32:
			return base_edu_points + Math.get_max([(base_character.app * 2), (base_character.dex * 2)])
		33:
			return base_edu_points + (base_character.app * 2)
		34:
			return base_edu_points + (base_character.app * 2)
		35:
			return base_edu_points + (base_character.edu * 2)
		36:
			return base_edu_points + Math.get_max([(base_character.app * 2), (base_character.dex * 2)])
		37:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		38:
			return base_edu_points + (base_character.edu * 2)
		39:
			return base_edu_points + (base_character.edu * 2)
		40:
			return base_edu_points + (base_character.edu * 2)
		41:
			return base_edu_points + (base_character.app * 2)
		42:
			return base_edu_points + (base_character.dex * 2)
		43:
			return base_edu_points + (base_character.edu * 2)
		44:
			return base_edu_points + Math.get_max([(base_character.app * 2), (base_character.dex * 2), (base_character.str * 2)])
		45:
			return base_edu_points + (base_character.dex * 2)
		46:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		47:
			return base_edu_points + (base_character.dex * 2)
		48:
			return base_edu_points + (base_character.edu * 2)
		49:
			return base_edu_points + (base_character.app * 2)
		50:
			return base_edu_points + (base_character.edu * 2)
		51:
			return base_edu_points + (base_character.app * 2)
		52:
			return base_edu_points + Math.get_max([(base_character.app * 2), (base_character.dex * 2), (base_character.str * 2)])
		53:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		54:
			return base_edu_points + (base_character.edu * 2)
		55:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		56:
			return base_edu_points + (base_character.edu * 2)
		57:
			return base_edu_points + (base_character.edu * 2)
		58:
			return base_edu_points + Math.get_max([(base_character.dex * 2), (base_character.app * 2)])
		59:
			return base_edu_points + (base_character.app * 2)
		60:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		61:
			return base_edu_points + (base_character.app * 2)
		62:
			return base_edu_points + Math.get_max([(base_character.dex * 2), (base_character.app * 2)])
		63:
			return base_edu_points + (base_character.str * 2)
		64:
			return base_edu_points + (base_character.edu * 2)
		65:
			return base_edu_points + (base_character.edu * 2)
		66:
			return base_edu_points + (base_character.edu * 2)
		67:
			return base_edu_points + (base_character.edu * 2)
		68:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		69:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		70:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		71:
			return base_edu_points + (base_character.edu * 2)
		72:
			return base_edu_points + (base_character.edu * 2)
		73:
			return base_edu_points + (base_character.edu * 2)
		74:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		75:
			return base_edu_points + (base_character.app * 2)
		76:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		77:
			return base_edu_points + (base_character.edu * 2)
		78:
			return base_edu_points + Math.get_max([(base_character.dex * 2), (base_character.pow * 2)])
		79:
			return base_edu_points + (base_character.edu * 2)
		80:
			return base_edu_points + (base_character.edu * 2)
		81:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		82:
			return base_edu_points + (base_character.edu * 2)
		83:
			return base_edu_points + (base_character.edu * 2)
		84:
			return base_edu_points + (base_character.edu * 2)
		85:
			return base_edu_points + (base_character.edu * 2)
		86:
			return base_edu_points + (base_character.dex * 2)
		87:
			return base_edu_points + (base_character.edu * 2)
		88:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		89:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		90:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		91:
			return base_edu_points + (base_character.edu * 2)
		92:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		93:
			return base_edu_points + (base_character.app * 2)
		94:
			return base_edu_points + (base_character.edu * 2)
		95:
			return base_edu_points + (base_character.edu * 2)
		96:
			return base_edu_points + (base_character.edu * 2)
		97:
			return base_edu_points + (base_character.edu * 2)
		98:
			return base_edu_points + (base_character.edu * 2)
		99:
			return base_edu_points + (base_character.app * 2)
		100:
			return base_edu_points + (base_character.app * 2)
		101:
			return base_edu_points + Math.get_max([(base_character.dex * 2), (base_character.app * 2)])
		102:
			return base_edu_points + Math.get_max([(base_character.dex * 2), (base_character.app * 2)])
		103:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		104:
			return base_edu_points + Math.get_max([(base_character.dex * 2), (base_character.app * 2)])
		105:
			return base_edu_points + (base_character.edu * 2)
		106:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		107:
			return base_edu_points + Math.get_max([(base_character.str * 2), (base_character.dex * 2)])
		108:
			return base_edu_points + (base_character.edu * 2)
		109:
			return base_edu_points + (base_character.edu * 2)
		110:
			return base_edu_points + Math.get_max([(base_character.dex * 2), (base_character.app * 2)])
		111:
			return base_edu_points + (base_character.edu * 2)
		112:
			return base_edu_points + (base_character.edu * 2)
		113:
			return base_edu_points + Math.get_max([(base_character.app * 2), (base_character.pow * 2)])
		114:
			return base_edu_points + (base_character.edu * 2)
	
func get_pro_skills(prof_index: int):
	match prof_index:
		0:
			return ["信用评级", "任意", "任意", "任意", "任意", "任意", "任意", "任意", "任意"]
		1:
			return ["信用评级", "会计", "法律", "图书馆使用", "聆听", "说服", "侦查", "任意", "任意"]
		2:
			return ["信用评级", "攀爬", "闪避", "跳跃", "投掷", "侦查", "游泳", "任意", "任意"]
		3:
			return ["信用评级", "表演", "乔装", "格斗", "历史", "社交", "社交", "心理学", "任意"]
		4:
			return ["信用评级", "表演", "乔装", "汽车驾驶", "社交", "社交", "心理学", "任意", "任意"]
		5:
			return ["信用评级", "社交", "斗殴", "射击", "法律", "图书馆使用", "心理学", "潜行", "追踪"]
		6:
			return ["信用评级", "法律", "聆听", "医学", "外语", "精神分析", "心理学", "生物学", "化学"]
		7:
			return ["信用评级", "跳跃", "聆听", "博物学", "心理学", "动物学", "潜行", "追踪", "任意"]
		8:
			return ["信用评级", "估价", "艺术与手艺", "历史", "图书馆使用", "外语", "社交", "侦查", "任意"]
		9:
			return ["信用评级", "会计", "估价", "汽车驾驶", "社交", "社交", "历史", "图书馆使用", "领航"]
		10:
			return ["信用评级", "估价", "考古学", "历史", "外语", "图书馆使用", "侦查", "机械维修", "领航", "科学"]
		11:
			return ["信用评级", "会计", "技术制图", "法律", "母语", "计算机使用", "图书馆使用", "说服", "心理学", "数学"]
		12:
			return ["信用评级", "艺术与手艺", "历史", "博物学", "社交", "外语", "心理学", "侦查", "任意", "任意"]
		13:
			return ["信用评级", "闪避", "斗殴", "急救", "社交", "社交", "聆听", "心理学", "潜行"]
		14:
			return ["信用评级", "攀爬", "跳跃", "斗殴", "骑术", "社交", "游泳", "投掷", "任意"]
		15:
			return ["信用评级", "文学", "历史", "图书馆使用", "博物学", "神秘学", "外语", "母语", "心理学", "任意"]
		16:
			return ["信用评级", "会计", "社交", "社交", "斗殴", "聆听", "心理学", "侦查", "任意"]
		17:
			return ["信用评级", "射击", "聆听", "侦查", "博物学", "领航", "外语", "生存", "生物学", "植物学", "潜行", "追踪"]
		18:
			return ["信用评级", "会计", "估价", "汽车驾驶", "历史", "图书馆使用", "母语", "外语", "社交"]
		19:
			return ["信用评级", "汽车驾驶", "电子学", "电气维修", "格斗", "射击", "社交", "法律", "心理学", "追踪", "潜行"]
		20:
			return ["信用评级", "闪避", "斗殴", "恐吓", "跳跃", "心理学", "侦查", "任意", "任意"]
		21:
			return ["信用评级", "会计", "估价", "艺术与手艺", "急救", "聆听", "外语", "心理学", "侦查", "任意"]
		22:
			return ["信用评级", "会计", "历史", "图书馆使用", "聆听", "外语", "社交", "心理学", "任意"]
		23:
			return ["信用评级", "计算机使用", "电气维修", "电子学", "图书馆使用", "数学", "侦查", "任意", "任意"]
		24:
			return ["信用评级", "计算机使用", "电气维修", "电子学", "图书馆使用", "侦查", "社交", "任意", "任意"]
		25:
			return ["信用评级", "闪避", "格斗", "射击", "急救", "博物学", "跳跃", "骑术", "生存", "投掷", "追踪"]
		26:
			return ["信用评级", "会计", "艺术与手艺", "艺术与手艺", "机械维修", "博物学", "侦查", "任意", "任意"]
		27:
			return ["信用评级", "乔装", "电气维修", "格斗", "射击", "锁匠", "机械维修", "潜行", "心理学"]
		28:
			return ["信用评级", "汽车驾驶", "电气维修", "机械维修", "格斗", "射击", "恐吓", "锁匠", "操作重型机械", "任意"]
		29:
			return ["信用评级", "汽车驾驶", "格斗", "射击", "社交", "社交", "心理学", "潜行", "侦查"]
		30:
			return ["信用评级", "估价", "攀爬", "电气维修", "机械维修", "聆听", "锁匠", "妙手", "潜行", "侦查"]
		31:
			return ["信用评级", "估价", "表演", "法律", "外语", "聆听", "社交", "社交", "心理学", "妙手"]
		32:
			return ["信用评级", "表演", "乔装", "估价", "社交", "格斗", "射击", "锁匠", "机械维修", "潜行", "心理学", "侦查"]
		33:
			return ["信用评级", "艺术与手艺", "社交", "社交", "斗殴", "手枪", "汽车驾驶", "聆听", "潜行", "任意"]
		34:
			return ["信用评级", "会计", "估价", "伪造文书", "历史", "社交", "图书馆使用", "侦查", "任意"]
		35:
			return ["信用评级", "会计", "估价", "伪造文书", "历史", "图书馆使用", "侦查", "妙手", "任意"]
		36:
			return ["信用评级", "射击", "聆听", "领航", "社交", "汽车驾驶", "驾驶", "心理学", "妙手", "侦查"]
		37:
			return ["信用评级", "攀爬", "社交", "格斗", "射击", "跳跃", "妙手", "潜行", "投掷"]
		38:
			return ["信用评级", "会计", "社交", "社交", "神秘学", "心理学", "侦查", "任意", "任意"]
		39:
			return ["信用评级", "社交", "社交", "汽车驾驶", "斗殴", "射击", "历史", "神秘学", "心理学", "潜行", "催眠"]
		40:
			return ["信用评级", "会计", "摄影", "艺术与手艺", "计算机使用", "图书馆使用", "机械维修", "心理学", "侦查", "任意"]
		41:
			return ["信用评级", "艺术与手艺", "射击", "外语", "骑术", "社交", "任意", "任意", "任意"]
		42:
			return ["信用评级", "潜水", "急救", "机械维修", "驾驶", "生物学", "侦查", "游泳", "任意"]
		43:
			return ["信用评级", "急救", "医学", "外语", "心理学", "生物学", "药学", "任意", "任意"]
		44:
			return ["信用评级", "攀爬", "跳跃", "聆听", "领航", "社交", "潜行", "任意", "任意"]
		45:
			return ["信用评级", "汽车驾驶", "社交", "社交", "聆听", "机械维修", "领航", "侦查", "任意"]
		46:
			return ["信用评级", "会计", "汽车驾驶", "聆听", "社交", "机械维修", "领航", "心理学", "任意"]
		47:
			return ["信用评级", "会计", "汽车驾驶", "电气维修", "话术", "机械维修", "领航", "侦查", "任意"]
		48:
			return ["信用评级", "会计", "历史", "母语", "社交", "社交", "心理学", "侦查", "任意"]
		49:
			return ["信用评级", "魅惑", "历史", "恐吓", "话术", "聆听", "母语", "说服", "心理学"]
		50:
			return ["信用评级", "伪造文书", "电气维修", "图书馆使用", "机械维修", "操作重型机械", "工程学", "物理学", "任意"]
		51:
			return ["信用评级", "艺术与手艺", "乔装", "社交", "社交", "聆听", "心理学", "任意", "任意"]
		52:
			return ["信用评级", "攀爬", "游泳", "射击", "历史", "跳跃", "博物学", "领航", "外语", "生存"]
		53:
			return ["信用评级", "耕作", "汽车驾驶", "社交", "机械维修", "博物学", "操作重型机械", "追踪", "任意"]
		54:
			return ["信用评级", "汽车驾驶", "斗殴", "射击", "法律", "说服", "潜行", "侦查", "任意"]
		55:
			return ["信用评级", "攀爬", "闪避", "汽车驾驶", "急救", "跳跃", "机械维修", "操作重型机械", "投掷"]
		56:
			return ["信用评级", "历史", "外语", "母语", "聆听", "社交", "社交", "心理学", "任意"]
		57:
			return ["信用评级", "外语", "图书馆使用", "医学", "说服", "生物学", "司法科学", "药学", "侦查"]
		58:
			return ["信用评级", "会计", "表演", "社交", "社交", "聆听", "心理学", "妙手", "侦查"]
		59:
			return ["信用评级", "格斗", "射击", "法律", "聆听", "社交", "社交", "心理学", "侦查"]
		60:
			return ["信用评级", "汽车驾驶", "格斗", "射击", "社交", "社交", "心理学", "任意", "任意"]
		61:
			return ["信用评级", "艺术与手艺", "社交", "社交", "步枪/霰弹枪", "历史", "外语", "领航", "骑术"]
		62:
			return ["信用评级", "艺术与手艺", "攀爬", "跳跃", "聆听", "锁匠", "妙手", "领航", "潜行", "任意"]
		63:
			return ["信用评级", "电气维修", "社交", "斗殴", "急救", "聆听", "机械维修", "心理学", "潜行"]
		64:
			return ["信用评级", "美术", "摄影", "社交", "历史", "图书馆使用", "母语", "心理学", "任意", "任意"]
		65:
			return ["信用评级", "表演", "历史", "聆听", "母语", "社交", "心理学", "潜行", "侦查"]
		66:
			return ["信用评级", "历史", "恐吓", "法律", "图书馆使用", "聆听", "母语", "说服", "心理学"]
		67:
			return ["信用评级", "计算机使用", "图书馆使用", "电气维修", "外语", "化学", "科学", "科学", "侦查", "任意"]
		68:
			return ["信用评级", "汽车驾驶", "电气维修", "格斗", "急救", "机械维修", "操作重型机械", "投掷", "任意"]
		69:
			return ["信用评级", "攀爬", "闪避", "电锯", "急救", "跳跃", "机械维修", "博物学", "生物学", "植物学", "投掷"]
		70:
			return ["信用评级", "攀爬", "地质学", "跳跃", "机械维修", "操作重型机械", "潜行", "侦查", "任意"]
		71:
			return ["信用评级", "会计", "法律", "图书馆使用", "社交", "社交", "心理学", "任意", "任意"]
		72:
			return ["信用评级", "会计", "图书馆使用", "外语", "母语", "任意", "任意", "任意", "任意"]
		73:
			return ["信用评级", "艺术与手艺", "攀爬", "汽车驾驶", "电气维修", "机械维修", "操作重型机械", "任意", "任意"]
		74:
			return ["信用评级", "会计", "射击", "领航", "急救", "社交", "社交", "心理学", "任意"]
		75:
			return ["信用评级", "艺术与手艺", "急救", "机械维修", "医学", "博物学", "社交", "任意", "任意"]
		76:
			return ["信用评级", "攀爬", "急救", "跳跃", "聆听", "领航", "外语", "生存", "追踪"]
		77:
			return ["信用评级", "会计", "估价", "考古学", "历史", "图书馆使用", "神秘学", "外语", "侦查"]
		78:
			return ["信用评级", "艺术与手艺", "社交", "聆听", "心理学", "任意", "任意", "任意", "任意"]
		79:
			return ["信用评级", "急救", "聆听", "医学", "社交", "心理学", "生物学", "化学", "侦查"]
		80:
			return ["信用评级", "人类学", "历史", "图书馆使用", "社交", "神秘学", "外语", "天文学", "任意", "克苏鲁神话"]
		81:
			return ["信用评级", "射击", "急救", "聆听", "博物学", "领航", "侦查", "生存", "追踪"]
		82:
			return ["信用评级", "人类学", "摄影", "历史", "图书馆使用", "神秘学", "外语", "心理学", "任意"]
		83:
			return ["信用评级", "会计", "急救", "外语", "图书馆使用", "社交", "心理学", "药学", "化学"]
		84:
			return ["信用评级", "摄影", "社交", "心理学", "化学", "潜行", "侦查", "任意", "任意"]
		85:
			return ["信用评级", "摄影", "攀爬", "社交", "外语", "心理学", "化学", "任意", "任意"]
		86:
			return ["信用评级", "电气维修", "机械维修", "领航", "操作重型机械", "驾驶", "天文学", "任意", "任意"]
		87:
			return ["信用评级", "会计", "电气维修", "聆听", "机械维修", "领航", "驾驶", "侦查", "任意"]
		88:
			return ["信用评级", "表演", "乔装", "射击", "法律", "聆听", "社交", "心理学", "侦查", "任意"]
		89:
			return ["信用评级", "斗殴", "射击", "急救", "社交", "法律", "心理学", "侦查", "汽车驾驶", "骑术"]
		90:
			return ["信用评级", "摄影", "乔装", "法律", "图书馆使用", "社交", "心理学", "侦查", "任意"]
		91:
			return ["信用评级", "图书馆使用", "外语", "母语", "心理学", "任意", "任意", "任意", "任意"]
		92:
			return ["信用评级", "攀爬", "急救", "历史", "机械维修", "领航", "地质学", "侦查", "任意"]
		93:
			return ["信用评级", "艺术与手艺", "社交", "社交", "闪避", "心理学", "妙手", "潜行", "任意"]
		94:
			return ["信用评级", "外语", "聆听", "医学", "说服", "精神分析", "心理学", "生物学", "化学"]
		95:
			return ["信用评级", "会计", "图书馆使用", "聆听", "说服", "精神分析", "心理学", "任意", "任意"]
		96:
			return ["信用评级", "历史", "图书馆使用", "社交", "外语", "侦查", "科学", "科学", "科学"]
		97:
			return ["信用评级", "电气维修", "机械维修", "格斗", "射击", "急救", "领航", "驾驶", "生存", "游泳"]
		98:
			return ["信用评级", "急救", "机械维修", "博物学", "领航", "社交", "驾驶", "侦查", "游泳"]
		99:
			return ["信用评级", "会计", "社交", "社交", "汽车驾驶", "聆听", "心理学", "潜行", "妙手", "任意"]
		100:
			return ["信用评级", "科学", "科学", "科学", "计算机使用", "图书馆使用", "外语", "母语", "社交", "侦查"]
		101:
			return ["信用评级", "会计", "打字", "速记", "社交", "社交", "母语", "图书馆使用", "计算机使用", "心理学", "任意"]
		102:
			return ["信用评级", "会计", "社交", "社交", "电气维修", "聆听", "机械维修", "心理学", "侦查"]
		103:
			return ["信用评级", "攀爬", "游泳", "闪避", "格斗", "射击", "潜行", "生存", "急救", "机械维修", "外语"]
		104:
			return ["信用评级", "表演", "乔装", "射击", "聆听", "外语", "社交", "心理学", "妙手", "潜行"]
		105:
			return ["信用评级", "母语", "外语", "图书馆使用", "聆听", "科学", "科学", "科学", "任意", "任意"]
		106:
			return ["信用评级", "攀爬", "闪避", "电气维修", "机械维修", "格斗", "急救", "跳跃", "游泳", "潜水", "汽车驾驶", "骑术"]
		107:
			return ["信用评级", "攀爬", "格斗", "投掷", "聆听", "博物学", "神秘学", "侦查", "游泳", "生存"]
		108:
			return ["信用评级", "会计", "汽车驾驶", "社交", "历史", "神秘学", "心理学", "生物学", "化学"]
		109:
			return ["信用评级", "会计", "社交", "社交", "斗殴", "法律", "聆听", "操作重型机械", "心理学"]
		110:
			return ["信用评级", "会计", "艺术与手艺", "闪避", "聆听", "社交", "社交", "心理学", "任意"]
		111:
			return ["信用评级", "会计", "母语", "外语", "法律", "图书馆使用", "计算机使用", "聆听", "社交", "任意", "任意"]
		112:
			return ["信用评级", "会计", "外语", "法律", "社交", "社交", "心理学", "任意", "任意"]
		113:
			return ["信用评级", "历史", "社交", "社交", "心理学", "潜行", "任意", "任意", "任意"]
		114:
			return ["信用评级", "驯兽", "会计", "闪避", "急救", "博物学", "医学", "药学", "动物学"]

func get_pro_skills_help(prof_index: int):
	# TODO needs to be reworded
	match prof_index:
		0:
			return "不多于8个本职技能。使用自定义职业前，请先咨询你的守秘人。"
		1:
			return "会计，法律，图书馆，聆听，说服，侦查，任意其他两项个人或时代特长。"
		2:
			return "攀爬，闪避，跳跃，投掷，侦查，游泳，任意两项其他个人或时代特长。"
		3:
			return "技艺(表演)，乔装，格斗，历史，两项社交技能(魅惑、话术、恐吓、说服)，心理学，任意一项其他个人或时代特长。"
		4:
			return "技艺(表演)，乔装，汽车驾驶，两项社交技能(魅惑、话术、恐吓、说服)，心理学，任意两项其他个人或时代特长(如骑乘或格斗)。"
		5:
			return "一项社交技能(魅惑、话术、恐吓、说服)，格斗(斗殴)，射击，法律，图书馆，心理学，潜行，追踪。"
		6:
			return "法律，聆听，医学，外语，精神分析，心理学，科学(生物学，化学)。"
		7:
			return "跳跃，聆听，博物学，心理学，科学(动物学)，潜行，追踪，任意一项其他个人或时代特长。"
		8:
			return "估价，技艺(任一)，历史，图书馆，外语，一项社交技能(魅惑、话术、恐吓、说服)，侦查，任意一项其他个人或时代特长。"
		9:
			return "会计，估价，汽车驾驶，两项社交技能(魅惑、话术、恐吓、说服)，历史，图书馆，导航。"
		10:
			return "估价，考古，历史，外语，图书馆，侦查，机械维修，导航或科学(任一：如化学、物理、地理等)。"
		11:
			return "会计，技艺(技术制图)，法律，母语，计算机或图书馆，说服，心理学，科学(数学)。"
		12:
			return "技艺(任一)，历史或博物学，一项社交技能(魅惑、话术、恐吓、说服)，外语，心理学，侦查，任意两项其他个人或时代特长。"
		13:
			return "闪避，格斗(斗殴)，急救，两项社交技能(魅惑、话术、恐吓、说服)，聆听，心理学，潜行。"
		14:
			return "攀爬，跳跃，格斗(斗殴)，骑乘，一项社交技能(魅惑、话术、恐吓、说服)，游泳，投掷，任意一项其他个人或时代特长。"
		15:
			return "技艺(文学)，历史，图书馆，博物学或神秘学，外语，母语，心理学，任意一项其他个人或时代特长。"
		16:
			return "会计，两项社交技能(魅惑、话术、恐吓、说服)，格斗(斗殴)，聆听，心理学，侦查，任意一项其他个人或时代特长。"
		17:
			return "射击，聆听或侦查，博物学，导航，外语或生存(任一)，科学(生物学或植物学)，潜行，追踪。"
		18:
			return "会计，估价，汽车驾驶，历史，图书馆，母语，外语，一项社交技能(魅惑、话术、恐吓、说服)。"
		19:
			return "汽车驾驶，电子学或电气维修，格斗或射击，一项社交技能(魅惑、话术、恐吓、说服)，法律，心理学，追踪，潜行。"
		20:
			return "闪避，格斗(斗殴)，恐吓，跳跃，心理学，侦查，任意两项其他个人或时代特长。"
		21:
			return "会计或估价，技艺(任一：如烹饪、裁缝、理发)，急救，聆听，外语，心理学，侦查，任意一项其他个人或时代特长。"
		22:
			return "会计，历史，图书馆，聆听，外语，一项社交技能(魅惑、话术、恐吓、说服)，心理学，任意一项其他技能。"
		23:
			return "计算机，电气维修，电子学、图书馆，科学(数学)，侦查，任意两项其他个人或时代特长。"
		24:
			return "计算机，电气维修，电子学，图书馆，侦查，一项社交技能(魅惑、话术、恐吓、说服)，任意两项其他技能。"
		25:
			return "闪避，格斗或射击，急救或博物学，跳跃，骑乘，生存(任一)，投掷，追踪。"
		26:
			return "会计，技艺(任二)，机械维修，博物学，侦查，任意两项其他个人或时代特长。"
		27:
			return "乔装，电气维修，格斗，射击，锁匠，机械维修，潜行，心理学。"
		28:
			return "汽车驾驶，电气维修或机械维修，格斗，射击，恐吓，锁匠，操作重型机械，任意一项其他个人或时代特长。"
		29:
			return "汽车驾驶，格斗，射击，两项社交技能(魅惑、话术、恐吓、说服)，心理学，潜行，侦查。"
		30:
			return "估价，攀爬，电气维修或机械维修，聆听，锁匠，妙手，潜行，侦查。"
		31:
			return "估价，技艺(表演)，法律或外语，聆听，两项社交技能(魅惑、话术、恐吓、说服)，心理学，妙手。"
		32:
			return "技艺(表演)或乔装，估价，一项社交技能(魅惑、话术、恐吓、说服)，格斗或射击，锁匠或机械维修，潜行，心理学，侦查。"
		33:
			return "技艺(任意)，两项社交技能(魅惑、话术、恐吓、说服)，格斗(斗殴)或射击(手枪)，汽车驾驶，聆听，潜行，任意一项其他个人或时代特长。"
		34:
			return "会计，估价，技艺(伪造)，历史，一项社交技能(魅惑、话术、恐吓、说服)，图书馆，侦查，任意一项其他技能。"
		35:
			return "会计，估价，技艺(伪造)，历史，图书馆，侦查，妙手，任意一项其他个人或时代特长(如计算机)。"
		36:
			return "射击，聆听，导航，一项社交技能(魅惑、话术、恐吓、说服)，汽车驾驶或驾驶(飞行器或船)，心理学，妙手，侦查。"
		37:
			return "攀爬，一项社交技能(魅惑、话术、恐吓、说服)，格斗，射击，跳跃，妙手，潜行，投掷。"
		38:
			return "会计，两项社交技能(魅惑、话术、恐吓、说服)，神秘学，心理学，侦查，任意其他两项其他个人特长。"
		39:
			return "两项社交技能(魅惑、话术、恐吓、说服)，汽车驾驶，格斗(斗殴)或射击，历史，神秘学，心理学，潜行。※经KP允许，可用催眠替换其中一项。"
		40:
			return "会计，技艺(摄影)，技艺(任一)，计算机或图书馆，机械维修，心理学，侦查，任意一项其他个人特长。"
		41:
			return "技艺(任一)，射击，外语，骑乘，一项社交技能(魅惑、话术、恐吓、说服)，任意三项其他个人或时代特长。"
		42:
			return "潜水，急救，机械维修，驾驶(船)，科学(生物)，侦查，游泳，任意一项其他个人或时代特长。"
		43:
			return "急救、医学、外语(拉丁文)、心理学、科学(生物学，制药)，任两种其他学术或个人特长。"
		44:
			return "攀爬，跳跃，聆听，导航，一项社交技能(魅惑、话术、恐吓、说服)，潜行，任意两项其他个人或时代特长。"
		45:
			return "汽车驾驶，两项社交技能(魅惑、话术、恐吓、说服)，聆听，机械维修，导航，侦查，任意一项其他个人或时代特长。"
		46:
			return "会计，汽车驾驶，聆听，一项社交技能(魅惑、话术、恐吓、说服)，机械维修，导航，心理学，任意一项其他个人或时代特长。"
		47:
			return "会计，汽车驾驶，电气维修，话术，机械维修，导航，侦查，任意一项其他个人或时代特长。"
		48:
			return "会计，历史，母语，两项社交技能(魅惑、话术、恐吓、说服)，心理学，侦查，任意一项其他个人或时代特长。"
		49:
			return "魅惑，历史，恐吓，话术，聆听，母语，说服，心理学。"
		50:
			return "技艺(技术制图)，电气维修，图书馆，机械维修，操作重型机械，科学(工程学，物理)，任意一项其他个人或时代特长。"
		51:
			return "技艺(表演类，如表演、演唱、喜剧等)，乔装，两项社交技能(魅惑、话术、恐吓、说服)，聆听，心理学，任意两项其他个人或时代特长。"
		52:
			return "攀爬或游泳，射击，历史，跳跃，博物学，导航，外语，生存。"
		53:
			return "技艺(耕作)，汽车驾驶(或运货马车)，一项社交技能(魅惑、话术、恐吓、说服)，机械维修，博物学，操作重型机械，追踪，任意一项其他个人或时代特长"
		54:
			return "汽车驾驶，格斗(斗殴)，射击，法律，说服，潜行，侦查，任意一项其他个人或时代特长。"
		55:
			return "攀爬，闪避，汽车驾驶，急救，跳跃，机械维修，操作重型机械，投掷。"
		56:
			return "历史，外语，母语，聆听，两项社交技能(魅惑、话术、恐吓、说服)，心理学，任意一项其他个人或时代特长。"
		57:
			return "外语(拉丁文)，图书馆，医学，说服，科学(生物学，鉴证，制药)，侦查。"
		58:
			return "会计，技艺(表演)，两项社交技能(魅惑、话术、恐吓、说服)，聆听，心理学，妙手，侦查。"
		59:
			return "格斗，射击，法律，聆听，两项社交技能(魅惑、话术、恐吓、说服)，心理学，侦查。"
		60:
			return "汽车驾驶，格斗，射击，两项社交技能(魅惑、话术、恐吓、说服)，心理学，任意两项其他个人或时代特长。"
		61:
			return "技艺(任一)，两项社交技能(魅惑、话术、恐吓、说服)，射击(步枪/霰弹枪)，历史，外语(任一)，导航，骑乘。"
		62:
			return "技艺(任一)，攀爬，跳跃，聆听，锁匠或妙手，导航，潜行，任意一项其他个人或时代特长。"
		63:
			return "电气维修，一项社交技能(魅惑、话术、恐吓、说服)，格斗(斗殴)，急救，聆听，机械维修，心理学，潜行。"
		64:
			return "技艺(艺术或摄影)，一项社交技能(魅惑、话术、恐吓、说服)，历史，图书馆，母语，心理学，任意两项其他个人或时代特长。"
		65:
			return "技艺(表演)，历史，聆听，母语，一项社交技能(魅惑、话术、恐吓、说服)，心理学，潜行，侦查。"
		66:
			return "历史，恐吓，法律，图书馆，聆听，母语，说服，心理学。"
		67:
			return "计算机或图书馆，电气维修，外语，科学(化学和任意两项)，侦查，任意一项其他个人特长。"
		68:
			return "汽车驾驶，电气维修，格斗，急救，机械维修，操作重型机械，投掷，任意一项其他个人或时代特长。"
		69:
			return "攀爬，闪避，格斗(链锯)，急救，跳跃，机械维修，博物学或科学(生物学或植物学)，投掷。"
		70:
			return "攀爬，科学(地质)，跳跃，机械维修，操作重型机械，潜行，侦查，任意一项其他个人或时代特长。"
		71:
			return "会计，法律，图书馆，两项社交技能(魅惑、话术、恐吓、说服)，心理学，两项其他技能。"
		72:
			return "会计，图书馆，外语，母语，任意四项其他个人特长或专业书籍主题。"
		73:
			return "技艺(木工、焊接、管道工等)，攀爬，汽车驾驶，电气维修，机械维修，操作重型机械，任意两项其他个人或时代或技术特长。"
		74:
			return "会计，射击，导航，急救，两项社交技能(魅惑、话术、恐吓、说服)，心理学，任意一项其他个人或时代特长。"
		75:
			return "技艺(任一)，急救，机械维修，医学，博物学，一项社交技能(魅惑、话术、恐吓、说服)，任意两项其他个人或时代特长。"
		76:
			return "攀爬，急救，跳跃，聆听，导航，外语，生存(阿尔卑斯或类似)，追踪。"
		77:
			return "会计，估价，考古，历史，图书馆，神秘学，外语，侦查。"
		78:
			return "技艺(乐器)，一项社交技能(魅惑、话术、恐吓、说服)，聆听，心理学，四项其他技能。"
		79:
			return "急救，聆听，医学，一项社交技能(魅惑、话术、恐吓、说服)，心理学，科学(生物学，化学)，侦查。"
		80:
			return "人类学，历史，图书馆，一项社交技能(魅惑、话术、恐吓、说服)，神秘学，外语，科学(天文)，任意一项其他个人或时代特长。※经KP允许 可以包含克苏鲁神话。"
		81:
			return "射击，急救，聆听，博物学，导航，侦查，生存(任一)，追踪。"
		82:
			return "人类学，技艺(摄影)，历史，图书馆，神秘学，外语，心理学，任意一项其他个人或时代特长。"
		83:
			return "会计，急救，外语(拉丁文)，图书馆，一项社交技能(魅惑、话术、恐吓、说服)，心理学，科学(制药，化学)。"
		84:
			return "技艺(摄影)，一项社交技能(魅惑、话术、恐吓、说服)，心理学，科学(化学)，潜行，侦查，任意两项其他个人或时代特长。"
		85:
			return "技艺(摄影)，攀爬，一项社交技能(魅惑、话术、恐吓、说服)，外语，心理学，科学(化学)，任意两项其他个人或时代特长。"
		86:
			return "电气维修，机械维修，导航，操作重型机械，驾驶(飞行器)，科学(天文)，任意两项其他个人或时代特长。"
		87:
			return "会计，电气维修，聆听，机械维修，导航，驾驶(飞行器)，侦查，任意一项其他个人或时代特长。"
		88:
			return "技艺(表演)或乔装，射击，法律，聆听，一项社交技能(魅惑、话术、恐吓、说服)，心理学，侦查，一项其他技能。"
		89:
			return "格斗(斗殴)，射击，急救，一项社交技能(魅惑、话术、恐吓、说服)，法律，心理学，侦查和下面的一种个人特长：汽车驾驶或骑乘。"
		90:
			return "技艺(摄影)，乔装，法律，图书馆，一项社交技能(魅惑、话术、恐吓、说服)，心理学，侦查，一项其他个人或时代特长(如计算机、锁匠、格斗、射击)。"
		91:
			return "图书馆，外语，母语，心理学，任意四项其他学术、时代或个人特长。"
		92:
			return "攀爬、急救、历史、机械维修、导航、科学(地质)，侦查，任意一项其他个人或时代特长。"
		93:
			return "技艺(任一)，两项社交技能(魅惑、话术、恐吓、说服)，闪避，心理学，妙手，潜行，任意一项其他个人或时代特长。"
		94:
			return "外语，聆听，医学，说服，精神分析，心理学，科学(生物学，化学)。"
		95:
			return "会计，图书馆，聆听，说服，精神分析，心理学，任意两项其他学术、个人或时代特长。"
		96:
			return "历史，图书馆，一项社交技能(魅惑、话术、恐吓、说服)，外语，侦查，任意三项其他学术领域。"
		97:
			return "电工或机械维修，格斗，射击，急救，导航，驾驶(船)，生存(海上)，游泳。"
		98:
			return "急救，机械维修，博物学，导航，一项社交技能(魅惑、话术、恐吓、说服)，驾驶(船)，侦查，游泳。"
		99:
			return "会计，两项社交技能(魅惑、话术、恐吓、说服)，汽车驾驶，聆听，心理学，潜行或妙手，一项其他技能。"
		100:
			return "任意三项科学专业领域，计算机或图书馆，外语，母语，一项社交技能(魅惑、话术、恐吓、说服)，侦查。"
		101:
			return "会计，技艺(打字或速记)，两项社交技能(魅惑、话术、恐吓、说服)，母语，图书馆或计算机，心理学，任意一项其他个人或时代特长。"
		102:
			return "会计，两项社交技能(魅惑、话术、恐吓、说服)，电气维修，聆听，机械维修，心理学，侦查。"
		103:
			return "攀爬或游泳，闪避，格斗，射击，潜行，生存，下面任选两项：急救、机械维修、外语。"
		104:
			return "技艺(表演)或乔装，射击，聆听，外语，一项社交技能(魅惑、话术、恐吓、说服)，心理学，妙手，潜行。"
		105:
			return "语言(母语或外语)，图书馆，聆听，三个学习的专业，任意两项其他个人或时代特长。"
		106:
			return "攀爬，闪避，电气维修或机械维修，格斗，急救，跳跃，游泳，下面任选一项：潜水、汽车驾驶、驾驶(任一)，骑乘。"
		107:
			return "攀爬，格斗或投掷，聆听，博物学，神秘学，侦查，游泳，生存(任一)。"
		108:
			return "会计，汽车驾驶，一项社交技能(魅惑、话术、恐吓、说服)，历史，神秘学，心理学，科学(生物学，化学)。"
		109:
			return "会计，两项社交技能(魅惑、话术、恐吓、说服)，格斗(斗殴)，法律，聆听，操作重型机械，心理学。"
		110:
			return "会计，技艺(任一)，闪避，聆听，两项社交技能(魅惑、话术、恐吓、说服)，心理学，任意一项其他个人或时代特长。"
		111:
			return "会计，语言，法律，图书馆或计算机，聆听，一项社交技能(魅惑、话术、恐吓、说服)，任意两项其他个人或时代特长。"
		112:
			return "会计，外语，法律，两项社交技能(魅惑、话术、恐吓、说服)，心理学，任意两项其他个人或时代特长。"
		113:
			return "历史，两项社交技能(魅惑、话术、恐吓、说服)，心理学，潜行，任意三项其他个人或时代特长。"
		114:
			return "驯兽，会计，闪避，急救，博物学，医学，科学(制药，动物学)。"

func get_skill_type(skill):
	if skills.keys().find(skill) != -1:
		return "普通"
	elif skills_art.keys().find(skill) != -1:
		return "艺术与手艺"
	elif skills_science.keys().find(skill) != -1:
		return "科学"
	elif skills_combat.keys().find(skill) != -1:
		return "格斗"
	elif skills_shooting.keys().find(skill) != -1:
		return "射击"
	elif skills_special.keys().find(skill) != -1:
		return "非常规技能"
	elif skills_social.keys().find(skill) != -1:
		return "社交"
	else:
		return ""

func get_skill_base_point(skill):
	if skills.keys().find(skill) != -1:
		return skills[skill]
	elif skills_art.keys().find(skill) != -1:
		return skills_art[skill]
	elif skills_science.keys().find(skill) != -1:
		return skills_science[skill]
	elif skills_combat.keys().find(skill) != -1:
		return skills_combat[skill]
	elif skills_shooting.keys().find(skill) != -1:
		return skills_shooting[skill]
	elif skills_special.keys().find(skill) != -1:
		return skills_special[skill]
	elif skills_social.keys().find(skill) != -1:
		return skills_social[skill]
	else:
		return -1
