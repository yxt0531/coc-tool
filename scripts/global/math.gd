extends Node

func get_max(array):
	var output_num = array[0]
	for number in array:
		if number > output_num:
			output_num = number
	return output_num
