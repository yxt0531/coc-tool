extends Node

var upnp

func _ready():
	upnp = UPNP.new()
	upnp.discover()
	
func forward_port(port):
	upnp.add_port_mapping(port, 0, "COCTool", "UDP")
	upnp.add_port_mapping(port, 0, "COCTool", "TCP")
	print("forwarding port: " + str(port))
	
func remove_port(port):
	upnp.delete_port_mapping(port, "UDP")
	upnp.delete_port_mapping(port, "TCP")
	print("port forwarding removed: " + str(port))
