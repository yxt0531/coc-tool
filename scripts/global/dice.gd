extends Node

func _init():
	randomize()
	print("dice ready")

func d4(dice_count: int = 1):
	return roll(4, dice_count)
	
func d6(dice_count: int = 1):
	return roll(6, dice_count)
	
func d8(dice_count: int = 1):
	return roll(8, dice_count)
	
func d20(dice_count: int = 1):
	return roll(20, dice_count)
	
func d100(dice_count: int = 1):
	return roll(100, dice_count)
	
func roll(max_number: int, dice_count: int = 1):
	print("rolling " + str(dice_count) + " d" + str(max_number) + " dice(s)")
	
	var new_roll
	
	var roll_time_count = 0
	var roll_number_total = 0
	
	while roll_time_count < dice_count:
		randomize()
		new_roll = randi() % max_number + 1
		roll_time_count += 1
		roll_number_total += new_roll
		print("dice rolled: " + str(new_roll))
	
	print("total dice count: " + str(roll_number_total))
	return roll_number_total
