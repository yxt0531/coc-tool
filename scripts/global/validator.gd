extends Node

var ip_regex
var ipv6_regex
var number_regex
	
func _ready():
	ip_regex = RegEx.new()
	ipv6_regex = RegEx.new()
	number_regex = RegEx.new()
	
	ip_regex.compile("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")
	ipv6_regex.compile("(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))")
	number_regex.compile("^[0-9]*$")

	
func is_ip_address(ip: String):
	if ip_regex.search(ip) or ipv6_regex.search(ip):
		return true
	else:
		return false

func is_number(input: String):
	if number_regex.search(input):
		return true
	else:
		return false
		
func contain_bbcode(text: String):
	if text.to_lower().find("[b]") != -1 or text.to_lower().find("[/b]") != -1:
		return true
	elif text.to_lower().find("[i]") != -1 or text.to_lower().find("[/i]") != -1:
		return true
	elif text.to_lower().find("[u]") != -1 or text.to_lower().find("[/u]") != -1:
		return true
	elif text.to_lower().find("[s]") != -1 or text.to_lower().find("[/s]") != -1:
		return true
	elif text.to_lower().find("[code]") != -1 or text.to_lower().find("[/code]") != -1:
		return true
	elif text.to_lower().find("[center]") != -1 or text.to_lower().find("[/center]") != -1:
		return true
	elif text.to_lower().find("[right]") != -1 or text.to_lower().find("[/right]") != -1:
		return true
	elif text.to_lower().find("[fill]") != -1 or text.to_lower().find("[/fill]") != -1:
		return true
	elif text.to_lower().find("[indent]") != -1 or text.to_lower().find("[/indent]") != -1:
		return true
	elif text.to_lower().find("[url]") != -1 or text.to_lower().find("[/url]") != -1:
		return true
	elif text.to_lower().find("[url=") != -1 or text.to_lower().find("[/url]") != -1:
		return true
	elif text.to_lower().find("[img]") != -1 or text.to_lower().find("[/img]") != -1:
		return true
	elif text.to_lower().find("[font=") != -1 or text.to_lower().find("[/font]") != -1:
		return true
	elif text.to_lower().find("[color=") != -1 or text.to_lower().find("[/color]") != -1:
		return true
	else:
		return false
