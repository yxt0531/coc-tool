extends Node

enum Type { HOST, CLIENT }

var character_id
var character

# connection detail
var type
var ip
var port

var connected_players = []
var connected_characters = {}

var remote_character

# for loading session
var session_id = null

func get_time_stamp():
	var hour
	var minute
	var second
	if OS.get_datetime().hour < 10:
		hour = "0" + str(OS.get_datetime().hour)
	else:
		hour = str(OS.get_datetime().hour)
		
	if OS.get_datetime().minute < 10:
		minute = "0" + str(OS.get_datetime().minute)
	else:
		minute = str(OS.get_datetime().minute)
		
	if OS.get_datetime().second < 10:
		second = "0" + str(OS.get_datetime().second)
	else:
		second = str(OS.get_datetime().second)
	
	return hour + ":" + minute + ":" + second

func append_to_connected_character(id, new_char):
	for key in connected_characters.keys():
		if connected_characters[key].name == new_char.name:
			print("character with same name exist, using old character data")
			connected_characters[id] = connected_characters[key].duplicate(true)
			connected_characters.erase(key)
			return key
	
	connected_characters[id] = new_char.duplicate(true)
	_init_new_character(id)
	return null

func _init_new_character(id):
	print("init new character: " + connected_characters[id].name)
	# update damage and body
	var str_siz = connected_characters[id].str + connected_characters[id].siz
	var dmg # to be appended
	var body # to be appended
	
	if str_siz >= 2 and str_siz <= 64:
		dmg = "-2"
		body = -2
	elif str_siz >= 65 and str_siz <= 84:
		dmg = "-1"
		body = -1
	elif str_siz >= 85 and str_siz <= 124:
		dmg = "0"
		body = 0
	elif str_siz >= 125 and str_siz <= 164:
		dmg = "+1D4"
		body = 1
	elif str_siz >= 165 and str_siz <= 204:
		dmg = "+1D6"
		body = 2
	elif str_siz >= 205 and str_siz <= 284:
		dmg = "+2D6"
		body = 3
	elif str_siz >= 285 and str_siz <= 364:
		dmg = "+3D6"
		body = 4
	elif str_siz >= 365 and str_siz <= 444:
		dmg = "+4D6"
		body = 5
	elif str_siz >= 445:
		dmg = "+5D6"
		body = 6
	
	connected_characters[id].dmg = dmg
	connected_characters[id].body = body
	
	# update mov
	var init_mov
	if connected_characters[id].dex < connected_characters[id].siz and connected_characters[id].str < connected_characters[id].siz:
		init_mov = 7
	elif connected_characters[id].dex > connected_characters[id].siz and connected_characters[id].str > connected_characters[id].siz:
		init_mov = 9
	else:
		init_mov = 8
		
	var final_mov # to be appended
	if connected_characters[id].age >= 40 and connected_characters[id].age <= 49:
		final_mov = init_mov - 1
	elif connected_characters[id].age >= 50 and connected_characters[id].age <= 59:
		final_mov = init_mov - 2
	elif connected_characters[id].age >= 60 and connected_characters[id].age <= 69:
		final_mov = init_mov - 3
	elif connected_characters[id].age >= 70 and connected_characters[id].age <= 79:
		final_mov = init_mov - 4
	elif connected_characters[id].age >= 80:
		final_mov = init_mov - 5
	else:
		final_mov = init_mov
		
	connected_characters[id].mov = final_mov

	# update hp
	connected_characters[id].hp_max = int((connected_characters[id].con + connected_characters[id].siz) / 10)
	connected_characters[id].hp = int((connected_characters[id].con + connected_characters[id].siz) / 10)
	
	# update san
	connected_characters[id].san_max = connected_characters[id].pow
	connected_characters[id].san = connected_characters[id].pow
	
	# update mp
	connected_characters[id].mp_max = int(connected_characters[id].pow / 5)
	connected_characters[id].mp = int(connected_characters[id].pow / 5)

func save_to_session(character_log: String, player_log: String, notes: String, session_name: String):
	if session_name == "":
		session_name = "游戏保存于 " + get_time_stamp()
		
	var session = {
		"name": session_name,
		"notes": notes,
		"characters": connected_characters,
		"character_log": character_log,
		"player_log": player_log
	}
	SessionDB.add_session(session)

func clear():
	connected_players.clear()
	connected_characters.clear()
	session_id = null
